<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LanguageSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('languages')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Русский'],
            ['id' => (string)Str::uuid(), 'title' => 'Английский'],
            ['id' => (string)Str::uuid(), 'title' => 'Польский'],
            ['id' => (string)Str::uuid(), 'title' => 'Немецкий'],
        ];

        DB::table('languages')->insert($data);
    }

}

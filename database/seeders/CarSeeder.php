<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CarSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('cars')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Легковой'],
            ['id' => (string)Str::uuid(), 'title' => 'До 3.5'],
            ['id' => (string)Str::uuid(), 'title' => 'До 7.5'],
            ['id' => (string)Str::uuid(), 'title' => 'Тягкач с прицепом'],
            ['id' => (string)Str::uuid(), 'title' => 'Грузовой'],
            ['id' => (string)Str::uuid(), 'title' => 'Грузовой с прицепом'],
            ['id' => (string)Str::uuid(), 'title' => 'Цистерна'],
            ['id' => (string)Str::uuid(), 'title' => 'Автобус'],
        ];

        DB::table('cars')->insert($data);
    }

}

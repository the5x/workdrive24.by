<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentationVacancyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('documentation_vacancy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('documentation_id');
            $table->uuid('vacancy_id');

            $table->foreign('documentation_id')->references('id')->on('documentations')->onDelete('cascade');
            $table->foreign('vacancy_id')->references('id')->on('vacancies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('documentation_vacancy');
    }

}

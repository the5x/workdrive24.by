<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenseVacancyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('license_vacancy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('license_id');
            $table->uuid('vacancy_id');

            $table->foreign('license_id')->references('id')->on('licenses')->onDelete('cascade');
            $table->foreign('vacancy_id')->references('id')->on('vacancies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('license_vacancy');
    }

}

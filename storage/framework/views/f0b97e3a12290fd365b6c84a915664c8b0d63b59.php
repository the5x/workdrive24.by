<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <a class="breadcrumbs" href="<?php echo e(route('vacancies')); ?>">На страницу вакансий</a>
        <section class="description">
            <h3 class="section__title section__title_sm">
                <?php echo e($vacancy->title); ?>


                <?php if(Auth::id() === $vacancy->user_id || isset(Auth::user()->is_admin)): ?>
                    <a class="link__ico" href="<?php echo e(route('vacancy.edit', ['id' => $vacancy->id])); ?>">
                        <img class="link__ico-pic" src="<?php echo e(URL::asset('assets/images/ico-edit.svg')); ?>" alt=""/>
                    </a>
                <?php endif; ?>
            </h3>

            <div class="price__column">
                <?php echo $__env->make('includes.start-end-salary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>

            <br>

            <?php if(isset(Auth::user()->is_admin)): ?>

                <?php if(\Carbon\Carbon::now() < $vacancy->deadline): ?>
                    <form action="<?php echo e(route('vacancy.free', ['id' => $vacancy->id])); ?>" method="post">
                        <?php echo method_field('PUT'); ?>
                        <?php echo csrf_field(); ?>

                        <button class="btn__premium">
                            <img class="btn__premium-ico btn__premium-ico_opacity"
                                 src="<?php echo e(URL::asset('assets/images/ico-premium.svg')); ?>" alt=""/>
                            <span class="btn__premium-text btn__premium-text_opacity">Снять выделение с вакансии</span>
                        </button>
                    </form>
                <?php else: ?>
                    <form action="<?php echo e(route('vacancy.premium', ['id' => $vacancy->id])); ?>" method="post">
                        <?php echo method_field('PUT'); ?>
                        <?php echo csrf_field(); ?>

                        <button class="btn__premium">
                            <img class="btn__premium-ico"
                                 src="<?php echo e(URL::asset('assets/images/ico-premium.svg')); ?>" alt=""/>
                            <span class="btn__premium-text">Выделить вакансию на 1 неделю</span>
                        </button>
                    </form>
                <?php endif; ?>
            <?php endif; ?>

            <br>

            <div class="description__grid">
                <div class="description__column">
                    <div class="description__company">

                        <div class="description__profile-cover">
                            <?php if(isset($vacancy->company->photo) && !empty($vacancy->company->photo)): ?>
                                <img class="profile__pic" src="<?php echo e(asset('uploads/' . $vacancy->company->photo )); ?>"
                                     alt="">
                            <?php else: ?>
                                <div class="profile__column profile__column_sm profile__column_border"></div>
                            <?php endif; ?>
                        </div>

                        <?php if($vacancy->is_direct_employer): ?>
                        <div class="description__badge">
                            <?php echo app('translator')->get('summary.direct_employer'); ?>
                        </div>
                        <?php endif; ?>

                        <div class="description__company-column">
                            <span class="descripktion__company-name">
                                <a class="vacancy__meta-link"
                                   href="<?php echo e(route('company.show', ['id' => $vacancy->company->id])); ?>"><?php echo e($vacancy->company->title); ?></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="description__column description__column_fl2">
                    <h5 class="description__subtitle">Требования</h5>
                    <ul class="description__list">
                        <li class="description__item">
                            Удостоверение:
                            <br/>
                            <?php $__currentLoopData = $vacancy->licenses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $licence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($licence->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Дополнительные документы:
                            <br/>
                            <?php $__currentLoopData = $vacancy->documentations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $documentation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($documentation->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Место жительства:
                            <br/>
                            <?php $__currentLoopData = $vacancy->residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($residence->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Тип авто:
                            <br/>
                            <?php $__currentLoopData = $vacancy->cars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $car): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($car->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Стажировка:
                            <br/>
                            <?php $__currentLoopData = $vacancy->internships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($internship->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Языки:
                            <br/>
                            <?php $__currentLoopData = $vacancy->languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($language->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Помощь с документами:
                            <br/>
                            <?php $__currentLoopData = $vacancy->documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($document->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                        <li class="description__item">
                            Опыт работы:
                            <br/>
                            <?php $__currentLoopData = $vacancy->experiences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <span class="description__item-option"><?php echo e($experience->title); ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </li>
                    </ul>
                </div>
                <div class="description__column">
                    <div class="description__user">
                        <strong class="description__subtitle">Контактное лицо</strong>
                        <?php if(isset($vacancy->employer->firstname, $vacancy->employer->lastname)): ?>
                            <span class="description__user-text">
                                <?php echo e($vacancy->employer->firstname); ?> <?php echo e($vacancy->employer->lastname); ?>

                            </span>
                        <?php else: ?>
                            <em class="description__user-text">Нет данных</em>
                        <?php endif; ?>

                        <strong class="description__subtitle">Номер телефона</strong>

                        <?php if(isset( $vacancy->employer->phone )): ?>
                            <a href="tel:<?php echo e($vacancy->employer->phone); ?>"
                               class="description__user-text description__user-link">
                                <?php echo e($vacancy->employer->phone); ?>

                            </a>
                        <?php else: ?>
                            <em class="description__user-text">Нет данных</em>
                        <?php endif; ?>

                        <strong class="description__subtitle">Электронная почта</strong>
                        <a href="mailto:<?php echo e($vacancy->employer->email); ?>"
                           class="description__user-text description__user-text_mb description__user-link"><?php echo e($vacancy->employer->email); ?></a>
                    </div>
                </div>
            </div>

            <div class="description__other">
                <h3 class="description__subtitle">Дополнительные данные</h3>
                <p class="description__text"><?php echo e($vacancy->information); ?></p>
            </div>

        </section>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.child', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/vacancies/show.blade.php ENDPATH**/ ?>
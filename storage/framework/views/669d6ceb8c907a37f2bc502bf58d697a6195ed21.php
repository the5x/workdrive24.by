<div class="vacancy__item <?php echo e(\Carbon\Carbon::now() < $vacancy->deadline ? 'summary__item_yellow' : ''); ?>">
    <div class="vacancy__cover"
         style="background-image: url(<?php echo e(asset('uploads/' .  $vacancy->company->photo)); ?>)"></div>

    <?php if($vacancy->is_direct_employer): ?>
        <div class="description__badge">
            <?php echo app('translator')->get('summary.direct_employer'); ?>
        </div>
    <?php endif; ?>

    <a href="<?php echo e(route('vacancy.show', ['id' => $vacancy->id])); ?>" class="vacancy__link"><?php echo e($vacancy->title); ?></a>

    <small class="vacancy__meta">
        <a class="vacancy__meta-link" href="<?php echo e(route('company.show', ['id' => $vacancy->company->id])); ?>"><?php echo e($vacancy->company->title); ?></a>
    </small>
    <small class="vacancy__meta">
        <?php $__currentLoopData = $vacancy->residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <span class="vacancy__meta-city"><?php echo e($residence->title); ?></span>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </small>
    <div class="vacancy__data">
        <div class="vacancy__price">

            <div class="price__column">
                <?php echo $__env->make('includes.start-end-salary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>

        </div>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/vacancy.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>


    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>
            <h3 class="section__title section__title_sm">Обновить вакансию</h3>

            <form action="<?php echo e(route('vacancy.update', ['id' => $vacancy->id])); ?>" method="POST">
                <?php echo method_field('PUT'); ?>
                <?php echo csrf_field(); ?>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="title" class="admin__label"><?php echo app('translator')->get('summary.title'); ?></label>
                        <input class="admin__input" type="text" name="title"
                               value="<?php echo e(old('title') ?: $vacancy->title); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['start_salary'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="start_salary" class="admin__label"><?php echo app('translator')->get('summary.salary_from'); ?></label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="start_salary" value="<?php echo e(old('start_salary') ?: $vacancy->start_salary); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['end_salary'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="end_salary" class="admin__label"><?php echo app('translator')->get('summary.salary_to'); ?></label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="end_salary" value="<?php echo e(old('end_salary') ?: $vacancy->end_salary); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['company'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="company" class="admin__label"><?php echo app('translator')->get('summary.company'); ?></label>
                        <h4 class="filter__option-subtitle"><?php echo e($vacancy->company->title); ?></h4>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <div class="filter__option">

                            <?php $__errorArgs = ['license'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.driver_license'); ?></h4>
                            <?php $__currentLoopData = $licenses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $license): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($license->id); ?>"
                                        <?php echo e($vacancy->licenses->contains($license) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="checkbox" value="<?php echo e($license->id); ?>"
                                        name="license[]">
                                    <label for="<?php echo e($license->id); ?>"
                                           class="filter__option-title"><?php echo e($license->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">

                            <?php $__errorArgs = ['documentation'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.additional_document'); ?></h4>

                            <?php $__currentLoopData = $documentations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $documentation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($documentation->id); ?>"
                                        <?php echo e($vacancy->documentations->contains($documentation) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="checkbox" value="<?php echo e($documentation->id); ?>"
                                        name="documentation[]">
                                    <label for="<?php echo e($documentation->id); ?>"
                                           class="filter__option-title"><?php echo e($documentation->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>

                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            <?php $__errorArgs = ['residence'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.place_residence'); ?></h4>

                            <?php $__currentLoopData = $residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($residence->id); ?>"
                                        <?php echo e($vacancy->residences->contains($residence) ? 'checked' : ''); ?>

                                        class="filter__option-ui" type="checkbox"
                                        value="<?php echo e($residence->id); ?>"
                                        name="residence[]">
                                    <label for="<?php echo e($residence->id); ?>"
                                           class="filter__option-title"><?php echo e($residence->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            <?php $__errorArgs = ['language'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.language'); ?></h4>

                            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($language->id); ?>"
                                        <?php echo e($vacancy->languages->contains($language) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="checkbox" value="<?php echo e($language->id); ?>"
                                        name="language[]">
                                    <label for="<?php echo e($language->id); ?>"
                                           class="filter__option-title"><?php echo e($language->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <div class="filter__option">
                            <?php $__errorArgs = ['car'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.car_type'); ?></h4>

                            <?php $__currentLoopData = $cars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $car): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($car->id); ?>"
                                        <?php echo e($vacancy->cars->contains($car) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="checkbox" value="<?php echo e($car->id); ?>"
                                        name="car[]">
                                    <label for="<?php echo e($car->id); ?>"
                                           class="filter__option-title"><?php echo e($car->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            <?php $__errorArgs = ['experience'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.work_experience'); ?></h4>

                            <?php $__currentLoopData = $experiences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($experience->id); ?>"
                                        <?php echo e($vacancy->experiences->contains($experience) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="radio" value="<?php echo e($experience->id); ?>"
                                        name="experience">
                                    <label for="<?php echo e($experience->id); ?>"
                                           class="filter__option-title"><?php echo e($experience->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            <?php $__errorArgs = ['internship'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.internship'); ?></h4>

                            <?php $__currentLoopData = $internships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($internship->id); ?>"
                                        <?php echo e($vacancy->internships->contains($internship) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="radio" value="<?php echo e($internship->id); ?>"
                                        name="internship">
                                    <label for="<?php echo e($internship->id); ?>"
                                           class="filter__option-title"><?php echo e($internship->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            <?php $__errorArgs = ['document'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <div class="message"><?php echo e($message); ?></div>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.help_with_document'); ?></h4>

                            <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="filter__option-column">
                                    <input
                                        id="<?php echo e($document->id); ?>"
                                        <?php echo e($vacancy->documents->contains($document) ? 'checked' : ''); ?>

                                        class="filter__option-ui"
                                        type="radio" value="<?php echo e($document->id); ?>"
                                        name="document">
                                    <label for="<?php echo e($document->id); ?>"
                                           class="filter__option-title"><?php echo e($document->title); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['information'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.additional_data'); ?></h4>
                        <textarea class="admin__textarea" name="information" id="" cols="30"
                                  rows="10"><?php echo e(old('information') ?: $vacancy->information); ?></textarea>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">

                        <?php $__errorArgs = ['is_direct_employer'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.direct_employer'); ?></h4>

                        <div class="admin__primary">
                            <input id="is_direct_employer" onclick="chooseDirectEmployer();" value="<?php echo e($vacancy->is_direct_employer); ?>" <?php echo e($vacancy->is_direct_employer ? 'checked' : ''); ?> class="admin__input admin__input_inline js_is_direct_employer" type="checkbox" name="is_direct_employer" />

                            <label for="is_direct_employer" class="admin__label admin__label_inline"><?php echo app('translator')->get('summary.direct_employer'); ?></label>
                        </div>

                        <script>

                            function chooseDirectEmployer() {
                                const SELECTED = 1;
                                const NOT_SELECTED = 0;

                                const directEmployerElement = document.querySelector('.js_is_direct_employer');

                                directEmployerElement.checked ?
                                    directEmployerElement.value = SELECTED :
                                    directEmployerElement.value = NOT_SELECTED;
                            }

                        </script>
                    </div>
                </div>

                <button class="filter__button" type="submit"><?php echo app('translator')->get('summary.update_vacancy'); ?></button>
            </form>

            <form action="<?php echo e(route('vacancy.delete', ['id' => $vacancy->id])); ?>" method="POST">
                <?php echo method_field('DELETE'); ?>
                <?php echo csrf_field(); ?>

                <button class="simple__button" type="submit"><?php echo app('translator')->get('summary.delete_vacancy'); ?></button>
            </form>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/vacancies/edit.blade.php ENDPATH**/ ?>
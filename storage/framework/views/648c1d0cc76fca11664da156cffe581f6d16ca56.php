<?php $__env->startSection('hero'); ?>

    <div class="hero__box">
        <div class="hero__slider">
            <?php if(auth()->guard('applicant')->check()): ?>
                <a href="<?php echo e(route('summary.create')); ?>" class="hero__link hero__link_yellow"><?php echo app('translator')->get('slider.create_cv'); ?></a>
            <?php endif; ?>
            <?php if(auth()->guard('employer')->check()): ?>
                <a href="<?php echo e(route('company.create')); ?>" class="hero__link hero__link_yellow"><?php echo app('translator')->get('slider.create_company'); ?></a>
                <a href="<?php echo e(route('vacancy.create')); ?>" class="hero__link hero__link_purple"><?php echo app('translator')->get('slider.create_vacancy'); ?></a>
            <?php endif; ?>
            <h1 class="hero__title"><?php echo app('translator')->get('slider.title'); ?></h1>
        </div>

        <div class="statistics">
            <div class="statistics__column statistics__column_summary">
                <strong class="statistics__title"><?php echo e($countSummaries); ?></strong>
                <span class="statistics__subtitle"><?php echo app('translator')->get('slider.summaries'); ?></span>
            </div>
            <div class="statistics__column statistics__column_job">
                <strong class="statistics__title"><?php echo e($countVacancies); ?></strong>
                <span class="statistics__subtitle"><?php echo app('translator')->get('slider.vacancies'); ?></span>
            </div>
            <div class="statistics__column statistics__column_company">
                <strong class="statistics__title"><?php echo e($countCompanies); ?></strong>
                <span class="statistics__subtitle statistics__subtitle_mb"><?php echo app('translator')->get('slider.companies'); ?></span>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('vacancy'); ?>
    <div class="vacancy__list">
        <?php $__currentLoopData = $vacancies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vacancy): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php echo $__env->make('includes.vacancy', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('summary'); ?>
    <div class="summary__list">
        <?php $__currentLoopData = $summaries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $summary): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="summary__item">
                <?php echo $__env->make('includes.summary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('partners'); ?>

    <div class="partners__list">
        <?php if(empty($logos)): ?>

        <?php else: ?>
            <?php $__currentLoopData = $logos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $logo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="partners__item">
                    <a class="partners__item-link" href="<?php echo e(route('company.show', ['id' => $logo->company->id])); ?>">
                        <img class="partners__item-link-pic" src="<?php echo e(asset('uploads/' .  $logo->company->photo)); ?>"
                             alt="<?php echo e($logo->company->title); ?>">
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('about'); ?>
    <h3 class="section__title"><?php echo app('translator')->get('title.about_us'); ?></h3>
    <p class="about__info"><?php echo app('translator')->get('about.content'); ?></p>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/pages/welcome.blade.php ENDPATH**/ ?>
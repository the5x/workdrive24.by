<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>
        <div class="cascade__grid">
            <div class="cascade__column cascade__column_mw">
                <div class="filter">
                    <h3 class="section__title section__title_sm"><?php echo app('translator')->get('summary.selection'); ?></h3>
                    <form action="<?php echo e(route('vacancies')); ?>" method="GET">
                        <?php echo $__env->make('includes.filter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <button class="filter__button" type="submit"><?php echo app('translator')->get('summary.find_vacancies'); ?></button>

                        <?php if( array_key_exists('query', parse_url( Request::fullUrl() )) ): ?>
                            <a href="<?php echo e(route('vacancies')); ?>" class="filter__reset"><?php echo app('translator')->get('summary.reset'); ?></a>
                        <?php else: ?>
                            <button class="filter__reset" type="reset"><?php echo app('translator')->get('summary.reset'); ?></button>
                        <?php endif; ?>

                    </form>
                </div>
            </div>
            <div class="cascade__column cascade__column_ml">
                <h3 class="section__title section__title_sm"><?php echo app('translator')->get('summary.vacancies'); ?></h3>

                <?php $__currentLoopData = $vacancies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vacancy): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div
                        class="summary__item summary__item_full <?php echo e(\Carbon\Carbon::now() < $vacancy->deadline ? 'summary__item_yellow' : ''); ?>">
                        <?php echo $__env->make('includes.vacancy_small', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php echo e($vacancies->appends(request()->query())->onEachSide(1)->links()); ?>


            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.child', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/vacancies/all.blade.php ENDPATH**/ ?>
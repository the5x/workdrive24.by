<?php if(isset($vacancy->start_salary)): ?>
    <small class="price__currency">от</small>
    <strong class="price__number"><?php echo e($vacancy->start_salary); ?> BYN</strong>
<?php endif; ?>

<?php if(isset($vacancy->end_salary)): ?>
    <small class="price__currency">до</small>
    <strong class="price__number"><?php echo e($vacancy->end_salary); ?> BYN</strong>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/start-end-salary.blade.php ENDPATH**/ ?>
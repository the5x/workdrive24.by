<div class="admin__grid">
    <div class="admin__column">
        <div class="filter__option">

            <?php $__errorArgs = ['license'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.driver_license'); ?></h4>

            <?php $__currentLoopData = $licenses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $license): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(is_array(old('license')) && in_array($license->id, old('license')) ? 'checked ' : ''); ?>

                        id="<?php echo e($license->id); ?>" class="filter__option-ui" type="checkbox"
                        value="<?php echo e($license->id); ?>"
                        name="license[]">
                    <label for="<?php echo e($license->id); ?>"
                           class="filter__option-title"><?php echo e($license->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">

            <?php $__errorArgs = ['documentation'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.additional_document'); ?></h4>

            <?php $__currentLoopData = $documentations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $documentation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(is_array(old('documentation')) && in_array($documentation->id, old('documentation')) ? 'checked ' : ''); ?>

                        id="<?php echo e($documentation->id); ?>"
                        class="filter__option-ui" type="checkbox" value="<?php echo e($documentation->id); ?>"
                        name="documentation[]">
                    <label for="<?php echo e($documentation->id); ?>"
                           class="filter__option-title"><?php echo e($documentation->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

    </div>

    <div class="admin__column">
        <div class="filter__option">
            <?php $__errorArgs = ['residence'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.place_residence'); ?></h4>

            <?php $__currentLoopData = $residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(is_array(old('residence')) && in_array($residence->id, old('residence')) ? 'checked ' : ''); ?>

                        id="<?php echo e($residence->id); ?>"
                        class="filter__option-ui" type="checkbox" value="<?php echo e($residence->id); ?>"
                        name="residence[]">
                    <label for="<?php echo e($residence->id); ?>"
                           class="filter__option-title"><?php echo e($residence->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            <?php $__errorArgs = ['language'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.language'); ?></h4>

            <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(is_array(old('language')) && in_array($language->id, old('language')) ? 'checked ' : ''); ?>

                        id="<?php echo e($language->id); ?>"
                        class="filter__option-ui" type="checkbox" value="<?php echo e($language->id); ?>"
                        name="language[]">
                    <label for="<?php echo e($language->id); ?>"
                           class="filter__option-title"><?php echo e($language->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>
</div>

<div class="admin__grid">
    <div class="admin__column">
        <div class="filter__option">
            <?php $__errorArgs = ['car'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.car_type'); ?></h4>

            <?php $__currentLoopData = $cars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $car): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(is_array(old('car')) && in_array($car->id, old('car')) ? 'checked ' : ''); ?>

                        id="<?php echo e($car->id); ?>"
                        class="filter__option-ui" type="checkbox" value="<?php echo e($car->id); ?>"
                        name="car[]">
                    <label for="<?php echo e($car->id); ?>"
                           class="filter__option-title"><?php echo e($car->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            <?php $__errorArgs = ['experience'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.work_experience'); ?></h4>

            <?php $__currentLoopData = $experiences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(old('experience') === $experience->id ? 'checked' : ''); ?>

                        id="<?php echo e($experience->id); ?>"
                        class="filter__option-ui" type="radio" value="<?php echo e($experience->id); ?>"
                        name="experience">
                    <label for="<?php echo e($experience->id); ?>"
                           class="filter__option-title"><?php echo e($experience->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            <?php $__errorArgs = ['internship'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.internship'); ?></h4>

            <?php $__currentLoopData = $internships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(old('internship') === $internship->id ? 'checked' : ''); ?>

                        id="<?php echo e($internship->id); ?>"
                        class="filter__option-ui" type="radio" value="<?php echo e($internship->id); ?>"
                        name="internship">
                    <label for="<?php echo e($internship->id); ?>"
                           class="filter__option-title"><?php echo e($internship->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            <?php $__errorArgs = ['document'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
            <div class="message"><?php echo e($message); ?></div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

            <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.help_with_document'); ?></h4>

            <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="filter__option-column">
                    <input
                        <?php echo e(old('document') === $document->id ? 'checked' : ''); ?>

                        id="<?php echo e($document->id); ?>"
                        class="filter__option-ui" type="radio"
                        value="<?php echo e($document->id); ?>"
                        name="document">
                    <label for="<?php echo e($document->id); ?>"
                           class="filter__option-title"><?php echo e($document->title); ?></label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/summary-vacancy-options-when-create.blade.php ENDPATH**/ ?>
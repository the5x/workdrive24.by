<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>
            <h3 class="section__title section__title_sm">Управление премиальными аккаунтами</h3>

            <?php $__currentLoopData = $employers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $profile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="admin__grid">
                    <div class="admin__column  admin__column_border">
                        <span class="admin__text"><?php echo e($profile->email); ?></span>
                    </div>
                    <div class="admin__column admin__column_border">

                        <?php if(isset(Auth::user()->is_admin)): ?>

                            <?php if($profile instanceof \App\Models\Employer): ?>

                                <?php if($profile->is_premium): ?>
                                    <form action="<?php echo e(route('profile.premium', ['id' => $profile->id])); ?>" method="post">
                                        <?php echo method_field('PUT'); ?>
                                        <?php echo csrf_field(); ?>
                                        <button class="admin__btn-delete">Выключить премиум аккаунт</button>
                                    </form>
                                <?php else: ?>
                                    <form action="<?php echo e(route('profile.premium', ['id' => $profile->id])); ?>" method="post">
                                        <?php echo method_field('PUT'); ?>
                                        <?php echo csrf_field(); ?>

                                        <button class="admin__btn-delete">Включить премиум аккаунт</button>
                                    </form>
                                <?php endif; ?>

                            <?php endif; ?>

                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/profile/employers.blade.php ENDPATH**/ ?>
<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '654969725867560');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id=654969725867560&ev=PageView
&noscript=1"/>
        </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<header class="header">
    <div class="wrapper">
        <?php echo $__env->make('includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</header>
<section class="hero" style="background-image: url(<?php echo e(URL::asset('assets/images/slider.jpg')); ?>)">
    <div class="wrapper">
        <?php echo $__env->yieldContent('hero'); ?>
    </div>
</section>
<section class="banner">
    <div class="wrapper">
        <?php echo $__env->make('includes.banners', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</section>
<section class="partners">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="<?php echo e(route('company.all')); ?>"><?php echo app('translator')->get('title.companies'); ?></a></h3>
        <?php echo $__env->yieldContent('partners'); ?>
    </div>
</section>
<section class="vacancy">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="<?php echo e(route('vacancies')); ?>"><?php echo app('translator')->get('title.vacancies'); ?></a></h3>
        <?php echo $__env->yieldContent('vacancy'); ?>
    </div>
</section>
<section class="banner">
    <div class="wrapper">
        <?php echo $__env->make('includes.banners', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</section>
<section class="summary">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="<?php echo e(route('summaries')); ?>"><?php echo app('translator')->get('title.cv'); ?></a></h3>
        <?php echo $__env->yieldContent('summary'); ?>
    </div>
</section>
<section class="about">
    <div class="wrapper">
        <?php echo $__env->yieldContent('about'); ?>
    </div>
</section>
<footer class="footer">
    <div class="wrapper">
        <?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</footer>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/layouts/app.blade.php ENDPATH**/ ?>
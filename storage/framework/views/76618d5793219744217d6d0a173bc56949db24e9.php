<div class="header__box">
    <div class="logo">
        <a class="logo__link" href="<?php echo e(route('home')); ?>">
            <img class="logo__link-pic" src="<?php echo e(URL::asset('assets/images/logo.png')); ?>" alt="workdrive24">
        </a>
    </div>
    <div class="header__meta">
        <div>
            <?php if(Auth::guard('applicant')->check()): ?>

                <div class="header__meta-ui">
                    <a href="<?php echo e(route('profile.show', ['id' => Auth::guard('applicant')->user()->id])); ?>"
                       class="admin__btn-delete"><?php echo app('translator')->get('header.profile'); ?>
                    </a>

                    <form action="<?php echo e(route('logout')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <button class="admin__btn-delete"><?php echo app('translator')->get('header.logout'); ?></button>
                    </form>
                </div>

            <?php elseif(Auth::guard('employer')->check()): ?>

                <div class="header__meta-ui">
                    <a href="<?php echo e(route('profile.show', ['id' => Auth::guard('employer')->user()->id])); ?>"
                       class="admin__btn-delete"><?php echo app('translator')->get('header.profile'); ?>
                    </a>

                    <form action="<?php echo e(route('logout')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <button class="admin__btn-delete"><?php echo app('translator')->get('header.logout'); ?></button>
                    </form>
                </div>

            <?php elseif(Auth::check() && Auth::user()->is_admin): ?>

                <div class="header__meta-ui">
                    <div class="header__nav">
                        <span class="header__nav-link"><?php echo app('translator')->get('menu.managing_and_creating'); ?></span>

                        <nav class="header__nav-wrapper">
                            <ul class="header__list">
                                <li class="header__list-item">
                                    <a class="header__list-item-link header__list-item-link_yellow"
                                       href="<?php echo e(route('logo.index')); ?>"><?php echo app('translator')->get('menu.company_logos'); ?></a>
                                </li>

                                <li class="header__list-item">
                                    <a class="header__list-item-link header__list-item-link_yellow"
                                       href="<?php echo e(route('company.all')); ?>"><?php echo app('translator')->get('menu.companies'); ?></a>
                                </li>

                                <li class="header__list-item">
                                    <a class="header__list-item-link header__list-item-link_yellow"
                                       href="<?php echo e(route('employers.all')); ?>"><?php echo app('translator')->get('menu.premium_employers'); ?></a>
                                </li>
                            </ul>

                            <ul class="header__list">
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('license.index')); ?>"><?php echo app('translator')->get('menu.drivers_license'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('document.index')); ?>"><?php echo app('translator')->get('menu.additional_documents'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('residence.index')); ?>"><?php echo app('translator')->get('menu.place_of_residence'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('language.index')); ?>"><?php echo app('translator')->get('menu.languages'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('car.index')); ?>"><?php echo app('translator')->get('menu.car_type'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('experience.index')); ?>"><?php echo app('translator')->get('menu.work_experience'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link"
                                       href="<?php echo e(route('internship.index')); ?>"><?php echo app('translator')->get('menu.internship'); ?></a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="<?php echo e(route('documentation.index')); ?>"><?php echo app('translator')->get('menu.help_with_documents'); ?></a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <form action="<?php echo e(route('logout')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <button class="admin__btn-delete"><?php echo app('translator')->get('header.logout'); ?></button>
                    </form>
                </div>

            <?php elseif(Auth::check()): ?>

                <div class="header__meta-ui">
                    <form action="<?php echo e(route('logout')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <button class="admin__btn-delete"><?php echo app('translator')->get('header.logout'); ?></button>
                    </form>
                </div>

            <?php else: ?>

                <div class="header__meta-ui">
                    <a href="<?php echo e(route('login')); ?>" class="admin__btn-delete">
                        <?php echo app('translator')->get('header.login'); ?> / <?php echo app('translator')->get('header.registration'); ?>
                    </a>
                </div>

            <?php endif; ?>
        </div>

        <select class="header__select" onchange="location = this.value;">
            <option class="header__select-option" <?php echo e(session()->get('applocale') === 'ru' ? 'selected': ''); ?> value="<?php echo e(route('set.language', 'ru')); ?>">ru</option>
            <option class="header__select-option" <?php echo e(session()->get('applocale') === 'en' ? 'selected': ''); ?> value="<?php echo e(route('set.language', 'en')); ?>">en</option>
            <option class="header__select-option" <?php echo e(session()->get('applocale') === 'pl' ? 'selected': ''); ?> value="<?php echo e(route('set.language', 'pl')); ?>">pl</option>
        </select>

    </div>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/header.blade.php ENDPATH**/ ?>
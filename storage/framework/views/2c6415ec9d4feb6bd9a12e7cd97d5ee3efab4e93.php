<div class="footer__list">
        <div class="footer__item">
            <h5 class="footer__subtitle"><?php echo app('translator')->get('footer.employers'); ?></h5>
            <ul class="footer__nav">
                <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('vacancy.create')); ?>"><?php echo app('translator')->get('footer.post_a_job'); ?></a></li>
                <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('rates')); ?>"><?php echo app('translator')->get('footer.rates_and_services'); ?></a></li>
            </ul>
        </div>

    <?php if(Auth::guard('applicant')->check()): ?>
        <div class="footer__item">
            <h5 class="footer__subtitle"><?php echo app('translator')->get('footer.applicants'); ?></h5>
            <ul class="footer__nav">
                <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('summary.create')); ?>"><?php echo app('translator')->get('footer.post_a_cv'); ?></a></li>
            </ul>
        </div>
    <?php endif; ?>

    <div class="footer__item">
        <h5 class="footer__subtitle"><?php echo app('translator')->get('footer.info'); ?></h5>
        <ul class="footer__nav">
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('contacts')); ?>"><?php echo app('translator')->get('footer.contacts'); ?></a></li>
        </ul>
    </div>

    <div class="footer__item">
        <h5 class="footer__subtitle"><?php echo app('translator')->get('footer.portal_rules'); ?></h5>
        <ul class="footer__nav">
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('user.agreement')); ?>"><?php echo app('translator')->get('footer.portal_rules'); ?></a></li>
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('public.index')); ?>"><?php echo app('translator')->get('footer.public_contract'); ?></a></li>
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('privacy.index')); ?>"><?php echo app('translator')->get('footer.privacy_policy'); ?></a></li>
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="<?php echo e(route('resume.index')); ?>"><?php echo app('translator')->get('footer.гules_for_posting_vacancies_and_cv'); ?></a></li>
        </ul>
    </div>
</div>
<div class="copy">
    <a class="copy__link" href="https://nastarte.by/" target="_blank">Дизайн и разработка — NaStarte</a>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/footer.blade.php ENDPATH**/ ?>
<div class="summary__cover summary__cover_mb">
    <div class="vacancy__cover"
         style="background-image: url(<?php echo e(asset('uploads/' .  $vacancy->company->photo)); ?>)"></div>
</div>
<div class="summary__data">
    <?php if($vacancy->is_direct_employer): ?>
        <div class="description__badge">
            <?php echo app('translator')->get('summary.direct_employer'); ?>
        </div>
    <?php endif; ?>

    <a href="<?php echo e(route('vacancy.show', ['id' => $vacancy->id])); ?>"
       class="summary__data-link summary__data-link_mb">
        <?php echo e($vacancy->title); ?>

    </a>
    <div class="summary__meta">
        <div class="summary__meta-data">
            <small class="summary__meta-text">
                <a class="vacancy__meta-link"
                   href="<?php echo e(route('company.show', ['id' => $vacancy->company->id])); ?>"><?php echo e($vacancy->company->title); ?></a>
            </small>
        </div>
        <div class="summary__meta-data">
            <?php $__currentLoopData = $vacancy->residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <small class="summary__meta-text">
                    <?php echo e($residence->title); ?>

                </small>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<div class="price">
    <div class="price__column">
        <?php echo $__env->make('includes.start-end-salary', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <div class="price__column">
        <time class="summary__date"><?php echo e(date('d-m-Y', strtotime($vacancy->created_at))); ?></time>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/vacancy_small.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>

            <div class="necessarily">
                <strong class="necessarily__subtitle"><?php echo app('translator')->get('summary.necessarily_item_1'); ?></strong>
                <p class="necessarily__text">1. <?php echo app('translator')->get('summary.necessarily_item_2'); ?> <a href="<?php echo e(route('vacancy.create')); ?>"><?php echo app('translator')->get('summary.necessarily_item_2_link'); ?></a></p>
                <p class="necessarily__text">2. <?php echo app('translator')->get('summary.necessarily_item_3'); ?></p>
                <p class="necessarily__text">3. <?php echo app('translator')->get('summary.necessarily_item_4'); ?></p>
            </div>

            <h3 class="section__title section__title_sm"><?php echo app('translator')->get('summary.create_vacancy'); ?></h3>

            <form action="<?php echo e(route('vacancy.create')); ?>" method="POST">
                <?php echo csrf_field(); ?>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="title" class="admin__label"><?php echo app('translator')->get('summary.title'); ?></label>
                        <input class="admin__input" type="text" name="title" value="<?php echo e(old('title')); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['company'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="company" class="admin__label"><?php echo app('translator')->get('summary.company'); ?></label>
                        <select class="admin__input" name="company" id="company">
                            <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php echo e(old('company') === $company->id ? 'selected' : ''); ?> value="<?php echo e($company->id); ?>"><?php echo e($company->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['start_salary'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="start_salary" class="admin__label"><?php echo app('translator')->get('summary.salary_from'); ?>:</label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="start_salary" value="<?php echo e(old('start_salary')); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['end_salary'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="end_salary" class="admin__label"><?php echo app('translator')->get('summary.salary_to'); ?>:</label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="end_salary" value="<?php echo e(old('end_salary')); ?>"/>
                    </div>
                </div>

                <?php echo $__env->make('includes.summary-vacancy-options-when-create', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['information'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.additional_data'); ?></h4>
                        <textarea class="admin__textarea" name="information" cols="30"
                                  rows="10"><?php echo e(old('information')); ?></textarea>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">

                        <?php $__errorArgs = ['is_direct_employer'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.direct_employer'); ?></h4>

                        <div class="admin__primary">
                            <input id="is_direct_employer" onclick="chooseDirectEmployer();" value="0" class="admin__input admin__input_inline js_is_direct_employer" type="checkbox" name="is_direct_employer" />

                            <label for="is_direct_employer" class="admin__label admin__label_inline"><?php echo app('translator')->get('summary.direct_employer'); ?></label>
                        </div>

                        <script>

                            function chooseDirectEmployer() {
                                const SELECTED = 1;
                                const NOT_SELECTED = 0;

                                const directEmployerElement = document.querySelector('.js_is_direct_employer');

                                directEmployerElement.checked ?
                                    directEmployerElement.value = SELECTED :
                                    directEmployerElement.value = NOT_SELECTED;
                            }

                        </script>

                    </div>
                </div>

                <button class="filter__button" type="submit"><?php echo app('translator')->get('summary.create_vacancy'); ?></button>
            </form>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/vacancies/create.blade.php ENDPATH**/ ?>
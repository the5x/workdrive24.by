<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body>
<header class="header">
    <div class="wrapper">
        <?php echo $__env->make('includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</header>
<main>
    <?php echo $__env->yieldContent('content'); ?>
</main>
<footer class="footer">
    <div class="wrapper">
        <?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</footer>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/layouts/admin.blade.php ENDPATH**/ ?>
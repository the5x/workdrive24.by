<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>

        <section class="auth">
            <h3 class="section__title section__title_sm"><?php echo app('translator')->get('auth.login_for_users'); ?></h3>
            <br /><br />
            <form class="auth__form" action="<?php echo e(route('login.enter')); ?>" method="post">
                <?php echo csrf_field(); ?>
                <div class="auth__ui">
                    <div class="auth__ui-column">
                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <label for="email" class="auth__ui-label">Email</label>
                        <input class="auth__ui-field" type="email" name="email" value="<?php echo e(old('email')); ?>">
                    </div>
                    <div class="auth__ui-column">
                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <label for="password" class="auth__ui-label">Password</label>
                        <input class="auth__ui-field" type="password" name="password">
                    </div>
                    <button class="filter__button"><?php echo app('translator')->get('auth.login'); ?></button>
                </div>
            </form>

            <a class="auth__forgot-link" href="<?php echo e(route('forgot.index')); ?>"><?php echo app('translator')->get('auth.forgot_your_password'); ?></a>

            <div class="auth__choose">
                <a class="auth__choose-link auth__choose-link_yellow" href="<?php echo e(route('applicant.index')); ?>"><?php echo app('translator')->get('auth.registration_as_applicant'); ?></a>
                <a class="auth__choose-link auth__choose-link_purple" href="<?php echo e(route('employer.index')); ?>"><?php echo app('translator')->get('auth.registration_as_employer'); ?></a>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/pages/auth/login.blade.php ENDPATH**/ ?>
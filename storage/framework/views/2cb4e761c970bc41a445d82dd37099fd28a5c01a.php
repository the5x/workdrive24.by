<?php

    $banners = [
        ['link' => 'https://workdrive24.by/', 'image'=> 'default.jpg'],
    ];

    $randomBanner = empty($banners) ? [] : $banners[array_rand($banners, 1)];
?>

<?php if(empty($randomBanner)): ?>
    <a href="<?php echo e(route('rates')); ?>">
        <div class="rate__banner"></div>
    </a>
<?php else: ?>
    <a target="_blank" class="banner__link" href="<?php echo e($randomBanner['link']); ?>">
        <img class="banner__link-images" src="<?php echo e(URL::asset('assets/banners/' . $randomBanner['image'])); ?>" alt="">
    </a>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/banners.blade.php ENDPATH**/ ?>
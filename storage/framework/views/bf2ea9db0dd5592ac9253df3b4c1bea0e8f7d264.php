<div class="summary__cover summary__cover_mb">
    <?php if(isset($summary->applicant->photo) && !empty($summary->applicant->photo)): ?>
        <img class="summary__cover-pic summary__cover-pic_cover"
             src="<?php echo e(asset('uploads/' .  $summary->applicant->photo)); ?>"
             alt="">
    <?php else: ?>
        <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>"
             alt="">
    <?php endif; ?>
</div>
<div class="summary__data">
    <a href="<?php echo e(route('summary.show', ['id' => $summary->id])); ?>"
       class="summary__data-link summary__data-link_mb"><?php echo e($summary->title); ?></a>
    <div class="summary__meta">
        <?php $__currentLoopData = $summary->residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <small class="summary__meta-text"><?php echo e($residence->title); ?></small>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<div class="price">
    <div class="price__column">
        <time class="summary__date"><?php echo e(date('d-m-Y', strtotime($summary->created_at))); ?></time>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/summary.blade.php ENDPATH**/ ?>
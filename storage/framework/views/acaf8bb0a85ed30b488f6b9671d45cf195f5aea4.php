<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.driver_license'); ?></h4>

    <?php $__currentLoopData = $licenses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $license): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('license')) && in_array($license->id, request('license')) ? 'checked' : ''); ?>

                id="<?php echo e($license->id); ?>" class="filter__option-ui" type="checkbox" value="<?php echo e($license->id); ?>"
                name="license[]">
            <label for="<?php echo e($license->id); ?>"
                   class="filter__option-title"><?php echo e($license->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.additional_document'); ?></h4>

    <?php $__currentLoopData = $documentations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $documentation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('documentation')) && in_array($documentation->id, request('documentation')) ? 'checked' : ''); ?>

                id="<?php echo e($documentation->id); ?>" class="filter__option-ui" type="checkbox" value="<?php echo e($documentation->id); ?>"
                name="documentation[]">
            <label for="<?php echo e($documentation->id); ?>"
                   class="filter__option-title"><?php echo e($documentation->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>


<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.place_residence'); ?></h4>

    <?php $__currentLoopData = $residences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $residence): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('residence')) && in_array($residence->id, request('residence')) ? 'checked' : ''); ?>

                id="<?php echo e($residence->id); ?>" class="filter__option-ui" type="checkbox" value="<?php echo e($residence->id); ?>"
                name="residence[]">
            <label for="<?php echo e($residence->id); ?>"
                   class="filter__option-title"><?php echo e($residence->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.language'); ?></h4>

    <?php $__currentLoopData = $languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('language')) && in_array($language->id, request('language')) ? 'checked' : ''); ?>

                id="<?php echo e($language->id); ?>" class="filter__option-ui" type="checkbox" value="<?php echo e($language->id); ?>"
                name="language[]">
            <label for="<?php echo e($language->id); ?>"
                   class="filter__option-title"><?php echo e($language->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.car_type'); ?></h4>

    <?php $__currentLoopData = $cars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $car): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('car')) && in_array($car->id, request('car')) ? 'checked' : ''); ?>

                id="<?php echo e($car->id); ?>" class="filter__option-ui" type="checkbox" value="<?php echo e($car->id); ?>"
                name="car[]">
            <label for="<?php echo e($car->id); ?>"
                   class="filter__option-title"><?php echo e($car->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>


<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.work_experience'); ?></h4>

    <?php $__currentLoopData = $experiences; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('experience')) && $experience->id === request('experience') ? 'checked' : ''); ?>

                id="<?php echo e($experience->id); ?>" class="filter__option-ui" type="radio" value="<?php echo e($experience->id); ?>"
                name="experience">
            <label for="<?php echo e($experience->id); ?>"
                   class="filter__option-title"><?php echo e($experience->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.internship'); ?></h4>

    <?php $__currentLoopData = $internships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('internship')) && $internship->id === request('internship') ? 'checked' : ''); ?>

                id="<?php echo e($internship->id); ?>" class="filter__option-ui" type="radio" value="<?php echo e($internship->id); ?>"
                name="internship">
            <label for="<?php echo e($internship->id); ?>"
                   class="filter__option-title"><?php echo e($internship->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle"><?php echo app('translator')->get('summary.help_with_document'); ?></h4>

    <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="filter__option-column">
            <input
                <?php echo e(!empty(request('document')) && $document->id === request('document') ? 'checked' : ''); ?>

                id="<?php echo e($document->id); ?>" class="filter__option-ui" type="radio" value="<?php echo e($document->id); ?>"
                name="document">
            <label for="<?php echo e($document->id); ?>"
                   class="filter__option-title"><?php echo e($document->title); ?></label>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/includes/filter.blade.php ENDPATH**/ ?>
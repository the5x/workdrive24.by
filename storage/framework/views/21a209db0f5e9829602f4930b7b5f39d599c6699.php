<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>
            <h3 class="section__title section__title_sm"><?php echo app('translator')->get('company.create_company'); ?></h3>

            <form action="<?php echo e(route('company.save')); ?>" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['photo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="title" class="admin__label"><?php echo app('translator')->get('company.company_logo'); ?></label>
                        <small><?php echo app('translator')->get('company.photo_param'); ?></small>
                        <input class="admin__input admin__input-file" type="file" name="photo"
                               value="<?php echo e(old('photo')); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <label for="title" class="admin__label"><?php echo app('translator')->get('company.title'); ?></label>
                        <input class="admin__input" type="text" name="title" value="<?php echo e(old('title')); ?>"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <?php $__errorArgs = ['information'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <div class="message"><?php echo e($message); ?></div>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <label for="title" class="admin__label"><?php echo app('translator')->get('company.company_information'); ?></label>
                        <textarea class="admin__textarea" name="information" cols="30"
                                  rows="10"><?php echo e(old('information')); ?></textarea>
                    </div>
                </div>

                <button class="filter__button" type="submit"><?php echo app('translator')->get('company.create_company'); ?></button>
            </form>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/companies/create.blade.php ENDPATH**/ ?>
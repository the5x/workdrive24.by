<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <a class="breadcrumbs" href="<?php echo e(route('home')); ?>">Главная</a>
        <h3 class="section__title section__title_sm"><?php echo app('translator')->get('title.rates'); ?></h3>
        <div class="rate__grid">
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 <?php echo app('translator')->get('rate.days'); ?> — 40 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 <?php echo app('translator')->get('rate.days'); ?> — 70 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan"><?php echo app('translator')->get('rate.rate_1_subtitle'); ?></em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_1_list_1'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_1_list_2'); ?></li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">
                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 <?php echo app('translator')->get('rate.days'); ?> — 70 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 <?php echo app('translator')->get('rate.days'); ?> — 100 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan"><?php echo app('translator')->get('rate.rate_2_subtitle'); ?></em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_2_list_1'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_2_list_2'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_2_list_3'); ?></li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 <?php echo app('translator')->get('rate.days'); ?> — 50 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 <?php echo app('translator')->get('rate.days'); ?> — 80 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan"><?php echo app('translator')->get('rate.rate_3_subtitle'); ?></em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_3_list_1'); ?></li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 <?php echo app('translator')->get('rate.days'); ?> — 110 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 <?php echo app('translator')->get('rate.days'); ?> — 150 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan"><?php echo app('translator')->get('rate.rate_4_subtitle'); ?></em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_4_list_1'); ?></li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 <?php echo app('translator')->get('rate.days'); ?> — 110 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 <?php echo app('translator')->get('rate.days'); ?> — 150 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan"><?php echo app('translator')->get('rate.rate_5_subtitle'); ?></em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_5_list_1'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_5_list_2'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_5_list_3'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_5_list_4'); ?></li>
                        <li class="rate__plan-list-item"><?php echo app('translator')->get('rate.rate_5_list_5'); ?></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="rate__grid">
            <div class="rate__column">
                <a href="mailto:client@workdrive24.by" class="rate__plan-btn"><?php echo app('translator')->get('rate.write_to_us'); ?></a>
            </div>
        </div>


        <br><br><br>

        <section class="rate">
            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle"><?php echo app('translator')->get('rate.description_1_title'); ?></h5>
                    <p class="rate__text"><?php echo app('translator')->get('rate.description_1_subtitle'); ?></p>
                </div>
                <div class="rate__column rate__column_fl3">
                    <div class="summary__item summary__item_full">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_opacity-50">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_opacity-25">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>" alt="">
                        </div>
                        <div class="summary__data">
                            <span class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle"><?php echo app('translator')->get('rate.description_2_title'); ?></h5>
                    <p class="rate__text"><?php echo app('translator')->get('rate.description_2_subtitle'); ?></p>
                </div>
                <div class="rate__column rate__column_fl3">
                    <div class="summary__item summary__item_full summary__item_yellow">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_yellow">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_opacity-50">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="<?php echo e(URL::asset('assets/images/ico-user.png')); ?>" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle"><?php echo app('translator')->get('rate.description_3_title'); ?></h5>
                    <p class="rate__text"><?php echo app('translator')->get('rate.description_3_subtitle'); ?></p>
                </div>
                <div class="rate__column rate__column_fl3">
                    <div class="rate__banner"></div>
                </div>
            </div>

            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle"><?php echo app('translator')->get('rate.description_4_title'); ?></h5>
                    <p class="rate__text"><?php echo app('translator')->get('rate.description_4_subtitle'); ?></p>
                </div>
                <div class="rate__column rate__column_fl3">

                <div class="partners__list">
                    <div class="partners__item">
                        <a class="partners__item-link" href="#">
                            <img class="partners__item-link-pic" src="<?php echo e(URL::asset('assets/images/partners.png')); ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.child', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\workdrive24.by\resources\views/pages/rates.blade.php ENDPATH**/ ?>
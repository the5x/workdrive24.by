<?php


namespace App\Services;


use App\Repositories\CarRepository;

class CarService extends BaseOptionService {

    public function __construct(
        CarRepository $carRepository
    ) {
        parent::__construct($carRepository);
    }

}

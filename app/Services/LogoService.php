<?php


namespace App\Services;


use App\Repositories\LogoRepository;

class LogoService {

    private LogoRepository $logoRepository;

    public function __construct(
        LogoRepository $logoRepository
    ) {
        $this->logoRepository = $logoRepository;
    }

    public function clearLogoTable() {
        return $this->logoRepository
            ->clearLogoTable();
    }

    public function createLogo($companyId) {
        return $this->logoRepository
            ->createLogo($companyId);
    }

    public function all() {
        return $this->logoRepository
            ->all();
    }

}

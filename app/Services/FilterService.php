<?php


namespace App\Services;

use Illuminate\Pagination\LengthAwarePaginator;

class FilterService {

    private SummaryService $summaryService;
    private VacancyService $vacancyService;

    public function __construct(
        SummaryService $summaryService,
        VacancyService $vacancyService
    ) {
        $this->summaryService = $summaryService;
        $this->vacancyService = $vacancyService;
    }

    public function getSummariesByQuery(): LengthAwarePaginator {
        return $this->summaryService->getResultsBySearchQuery();
    }

    public function getVacanciesByQuery(): LengthAwarePaginator {
        return $this->vacancyService->getResultsBySearchQuery();
    }

}

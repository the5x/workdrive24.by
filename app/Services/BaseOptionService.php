<?php


namespace App\Services;


use App\Repositories\BaseOptionRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseOptionService {

    private BaseOptionRepository $modelRepository;

    public function __construct(
        BaseOptionRepository $modelRepository
    ) {
        $this->modelRepository = $modelRepository;
    }

    public function create(array $data): Model {
        return $this->modelRepository
            ->create($data);
    }

    public function all(): Collection {
        return $this->modelRepository
            ->all();
    }

    public function delete(string $id): bool {
        return $this->modelRepository
            ->delete($id);
    }

}

<?php


namespace App\Services;


use App\Repositories\InternshipRepository;

class InternshipService extends BaseOptionService {

    public function __construct(
        InternshipRepository $internshipRepository
    ) {
        parent::__construct($internshipRepository);
    }

}

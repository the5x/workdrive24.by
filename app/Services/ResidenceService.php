<?php


namespace App\Services;


use App\Repositories\ResidenceRepository;

class ResidenceService extends BaseOptionService {

    public function __construct(
        ResidenceRepository $residenceRepository
    ) {
        parent::__construct($residenceRepository);
    }

}

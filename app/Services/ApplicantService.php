<?php


namespace App\Services;


use App\Models\Applicant;
use App\Repositories\ApplicantRepository;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Hash;

class ApplicantService {

    private ApplicantRepository $applicantRepository;
    private UploadImageService $uploadImageService;
    private UserTransformer $userTransformer;

    public function __construct(
        ApplicantRepository $applicantRepository,
        UploadImageService $uploadImageService,
        UserTransformer $userTransformer
    ) {
        $this->applicantRepository = $applicantRepository;
        $this->uploadImageService = $uploadImageService;
        $this->userTransformer = $userTransformer;
    }

    public function create(array $data): Applicant {
        $user = $this->userTransformer->transform($data);

        return $this->applicantRepository
            ->create($user);
    }

    public function show(string $id): ?Applicant {
        return $this->applicantRepository
            ->show($id);
    }

    public function update(array $data, string $id): void {
        $this->applicantRepository->update($data, $id);

        if (isset($data['photo'])) {
            $fileName = $this->uploadImageService->savePhotoToDirectory($data['photo']);
            $this->applicantRepository->update(['photo' => $fileName], $id);
        }
    }

    public function updatePassword(array $data): bool {
        ['email' => $email, 'password' => $password] = $data;

        return $this->applicantRepository
            ->updatePassword($email, Hash::make($password));
    }

}

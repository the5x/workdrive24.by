<?php


namespace App\Services;

use Illuminate\Support\Str;

class GenerateToken {

    private const RANDOM_NUMBER = 30;

    public function generate(): string {
        return Str::random(self::RANDOM_NUMBER);
    }

}

<?php


namespace App\Services;


use App\Repositories\PasswordResetRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class PasswordResetService {

    private GenerateToken $tokenService;
    private PasswordResetRepository $passwordResetRepository;

    public function __construct(
        GenerateToken $tokenService,
        PasswordResetRepository $passwordResetRepository
    ) {
        $this->passwordResetRepository = $passwordResetRepository;
        $this->tokenService = $tokenService;
    }

    public function saveToken(array $data): Model {
        $tokenData = [
            'email' => $data['email'],
            'token' => $this->tokenService->generate(),
            'created_at' => Carbon::now(),
        ];

        return $this->passwordResetRepository
            ->saveToken($tokenData);
    }

    public function searchToken(array $tokenData) {
        return $this->passwordResetRepository
            ->searchToken($tokenData);
    }

    public function deleteTokenByEmail(string $email): bool {
        return $this->passwordResetRepository
            ->deleteTokenByEmail($email);
    }

}

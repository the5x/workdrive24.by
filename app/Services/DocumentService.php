<?php


namespace App\Services;


use App\Repositories\DocumentRepository;

class DocumentService extends BaseOptionService {

    public function __construct(
        DocumentRepository $documentRepository
    ) {
        parent::__construct($documentRepository);
    }

}

<?php


namespace App\Services;


use App\Models\Employer;
use App\Repositories\EmployerRepository;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Hash;

class EmployerService {

    const PREMIUM_ACCOUNT_TRUE = true;
    const PREMIUM_ACCOUNT_FALSE = false;
    private EmployerRepository $employerRepository;
    private UploadImageService $uploadImageService;
    private UserTransformer $userTransformer;

    public function __construct(
        EmployerRepository $employerRepository,
        UploadImageService $uploadImageService,
        UserTransformer $userTransformer
    ) {
        $this->employerRepository = $employerRepository;
        $this->uploadImageService = $uploadImageService;
        $this->userTransformer = $userTransformer;
    }

    public function create(array $data): Employer {
        $user = $this->userTransformer->transform($data);

        return $this->employerRepository
            ->create($user);
    }

    public function show(string $id): ?Employer {
        return $this->employerRepository
            ->show($id);
    }

    public function getAllOwnersCompanies() {
        return $this->employerRepository
            ->getAllOwnersCompanies();
    }

    public function update(array $data, string $id): void {
        $this->employerRepository->update($data, $id);

        if (isset($data['photo'])) {
            $fileName = $this->uploadImageService->savePhotoToDirectory($data['photo']);
            $this->employerRepository->update(['photo' => $fileName], $id);
        }
    }

    public function updatePassword(array $data): bool {
        ['email' => $email, 'password' => $password] = $data;

        return $this->employerRepository
            ->updatePassword($email, Hash::make($password));
    }

    public function isPremium(string $id): bool {
        $profile = $this->employerRepository->show($id);

        return $profile->is_premium ?
            $this->employerRepository->isPremium($id, self::PREMIUM_ACCOUNT_FALSE) :
            $this->employerRepository->isPremium($id, self::PREMIUM_ACCOUNT_TRUE);
    }

    public function all(string $orderBy) {
        return $this->employerRepository->all($orderBy);
    }

}

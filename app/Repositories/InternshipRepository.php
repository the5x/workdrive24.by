<?php


namespace App\Repositories;


use App\Models\Internship;

class InternshipRepository extends BaseOptionRepository {

    public function __construct(
        Internship $internshipModel
    ) {
        parent::__construct($internshipModel);
    }

}

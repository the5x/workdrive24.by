<?php


namespace App\Repositories;


use App\Models\Logo;

class LogoRepository {

    private Logo $logoModel;

    public function __construct(
        Logo $logoModel
    ) {
        $this->logoModel = $logoModel;
    }

    public function all() {
        return $this->logoModel
            ->with('company')
            ->get();
    }

    public function clearLogoTable() {
        return $this->logoModel::query()
            ->truncate();
    }

    public function createLogo($companyId) {
        return $this->logoModel
            ->create(['company_id' => $companyId]);
    }

}

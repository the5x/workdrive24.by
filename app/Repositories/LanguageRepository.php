<?php


namespace App\Repositories;


use App\Models\Language;

class LanguageRepository extends BaseOptionRepository {

    public function __construct(
        Language $languageModel
    ) {
        parent::__construct($languageModel);
    }

}

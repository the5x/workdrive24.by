<?php


namespace App\Repositories;


use App\Models\User;

class UserRepository {

    private User $userModel;

    public function __construct(
        User $userModel
    ) {
        $this->userModel = $userModel;
    }

    public function create(array $data): User {
        return $this->userModel
            ->create($data);
    }

    public function updatePassword(string $email, string $password): bool {
        return $this->userModel
            ->where('email', $email)
            ->update(['password' => $password]);
    }

}

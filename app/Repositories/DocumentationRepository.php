<?php


namespace App\Repositories;


use App\Models\Documentation;

class DocumentationRepository extends BaseOptionRepository {

    public function __construct(
        Documentation $documentationModel
    ) {
        parent::__construct($documentationModel);
    }

}

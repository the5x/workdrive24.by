<?php


namespace App\Repositories;


use App\Models\Residence;


class ResidenceRepository extends BaseOptionRepository {

    public function __construct(
        Residence $residenceModel
    ) {
        parent::__construct($residenceModel);
    }

}

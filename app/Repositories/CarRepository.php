<?php


namespace App\Repositories;


use App\Models\Car;

class CarRepository extends BaseOptionRepository {

    public function __construct(
        Car $carModel
    ) {
        parent::__construct($carModel);
    }

}

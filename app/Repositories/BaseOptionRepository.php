<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseOptionRepository {

    private Model $model;

    public function __construct(
        Model $model
    ) {
        $this->model = $model;
    }

    public function create(array $data): Model {
        return $this->model
            ->create($data);
    }

    public function all(): Collection {
        return $this->model
            ->orderBy('title', 'ASC')
            ->get();
    }

    public function delete(string $id): bool {
        return $this->model
            ->findOrFail($id)
            ->delete();
    }

}

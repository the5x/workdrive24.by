<?php

namespace App\Models;

use App\Builders\FilterBuilder;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Vacancy extends Model {

    use HasFactory, UuidTrait;

    protected $table = 'vacancies';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['title', 'information', 'is_direct_employer', 'start_salary', 'end_salary', 'user_id', 'company_id', 'created_at'];

    public function newEloquentBuilder($query): FilterBuilder {
        return new FilterBuilder($query);
    }

    public function employer(): BelongsTo {
        return $this->belongsTo(Employer::class, 'user_id');
    }

    public function company(): BelongsTo {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function licenses(): BelongsToMany {
        return $this->belongsToMany(License::class)->orderBy('title', 'ASC');
    }

    public function documentations(): BelongsToMany {
        return $this->belongsToMany(Documentation::class);
    }

    public function residences(): BelongsToMany {
        return $this->belongsToMany(Residence::class);
    }

    public function languages(): BelongsToMany {
        return $this->belongsToMany(Language::class);
    }

    public function cars(): BelongsToMany {
        return $this->belongsToMany(Car::class);
    }

    public function experiences(): BelongsToMany {
        return $this->belongsToMany(Experience::class);
    }

    public function internships(): BelongsToMany {
        return $this->belongsToMany(Internship::class);
    }

    public function documents(): BelongsToMany {
        return $this->belongsToMany(Document::class);
    }

}

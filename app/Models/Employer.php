<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employer extends Authenticatable {

    use HasFactory, Notifiable, UuidTrait;

    protected $guard = 'employer';
    protected $table = 'employers';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
        'email',
        'password',
        'phone',
        'photo',
        'firstname',
        'lastname',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function companies(): HasMany {
        return $this->hasMany(Company::class, 'user_id');
    }

    public function vacancies(): HasMany {
        return $this->hasMany(Vacancy::class, 'user_id');
    }

}

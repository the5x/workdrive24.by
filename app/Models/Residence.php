<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Residence extends Model {

    use HasFactory, UuidTrait;

    protected $table = 'residences';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['title'];
}

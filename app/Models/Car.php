<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model {

    use HasFactory, UuidTrait;

    protected $table = 'cars';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['title'];

}

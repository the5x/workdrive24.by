<?php

namespace App\Models;

use App\Builders\FilterBuilder;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Summary extends Model {

    use HasFactory;

    use HasFactory, Notifiable, UuidTrait;

    protected $table = 'summaries';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['title', 'information', 'user_id', 'created_at'];

    public function newEloquentBuilder($query): FilterBuilder {
        return new FilterBuilder($query);
    }

    public function applicant(): BelongsTo {
        return $this->belongsTo(Applicant::class, 'user_id');
    }

    public function licenses(): BelongsToMany {
        return $this->belongsToMany(License::class)->orderBy('title', 'ASC');
    }

    public function documentations(): BelongsToMany {
        return $this->belongsToMany(Documentation::class);
    }

    public function residences(): BelongsToMany {
        return $this->belongsToMany(Residence::class);
    }

    public function languages(): BelongsToMany {
        return $this->belongsToMany(Language::class);
    }

    public function cars(): BelongsToMany {
        return $this->belongsToMany(Car::class);
    }

    public function experiences(): BelongsToMany {
        return $this->belongsToMany(Experience::class);
    }

    public function internships(): BelongsToMany {
        return $this->belongsToMany(Internship::class);
    }

    public function documents(): BelongsToMany {
        return $this->belongsToMany(Document::class);
    }

}

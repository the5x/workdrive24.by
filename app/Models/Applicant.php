<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Applicant extends Authenticatable {

    use HasFactory, Notifiable, UuidTrait;

    protected $guard = 'applicant';
    protected $table = 'applicants';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
        'email',
        'password',
        'phone',
        'photo',
        'firstname',
        'lastname',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function summaries(): HasMany {
        return $this->hasMany(Summary::class, 'user_id');
    }

}

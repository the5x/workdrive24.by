<?php


namespace App\Transformers;


use Illuminate\Support\Facades\Hash;

class UserTransformer {

    public function transform(array $data): array {
        ['email' => $email, 'password' => $password] = $data;

        return ['email' => $email, 'password' => Hash::make($password)];
    }

}

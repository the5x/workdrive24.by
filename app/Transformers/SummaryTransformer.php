<?php


namespace App\Transformers;


use App\Common\UserGuard;
use Illuminate\Support\Facades\Auth;

class SummaryTransformer {

    public function transform(array $data): array {
        return [
            'title' => $data['title'],
            'information' => $data['information'],
            'user_id' => Auth::guard(UserGuard::APPLICANT)->id(),
        ];
    }

    public function updateTransform(array $data): array {
        return [
            'title' => $data['title'],
            'information' => $data['information'],
        ];
    }

}

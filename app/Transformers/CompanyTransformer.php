<?php


namespace App\Transformers;


use App\Common\UserGuard;
use Illuminate\Support\Facades\Auth;

class CompanyTransformer {

    public function transform(array $data): array {
        ['title' => $title, 'information' => $information] = $data;

        return [
            'title' => $title,
            'information' => $information,
            'user_id' => Auth::guard(UserGuard::EMPLOYER)->id(),
        ];
    }

    public function updateTransform(array $data): array {
        return [
            'title' => $data['title'],
            'information' => $data['information'],
        ];
    }

}

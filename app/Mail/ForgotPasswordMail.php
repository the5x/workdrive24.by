<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordMail extends Mailable {

    use Queueable, SerializesModels;

    public $token;

    public function __construct($token) {
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->subject("Восстановление пароля")
            ->from($address = 'pinroll@yandex.ru', $name = 'Workdrive')
            ->view('emails.forgot-password');
    }

}

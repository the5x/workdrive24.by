<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CompanyCreated extends Mailable {

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $company;

    public function __construct($company) {
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject("Новая компания на workdrive24.by")
            ->from($address = 'pinroll@yandex.ru', $name = 'Workdrive')
            ->view('emails.company-created', ['company' => $this->company]);
    }

}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable {

    use Queueable, SerializesModels;

    public function __construct() {
    }

    public function build() {
        return $this
            ->subject("Регистрация пользователя")
            ->from($address = 'pinroll@yandex.ru', $name = 'Workdrive')
            ->view('emails.registered');
    }

}

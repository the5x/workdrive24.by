<?php

namespace App\Listeners;

use App\Events\ForgotPasswordEvent;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordListener {

    public function handle(ForgotPasswordEvent $event): void {
        Mail::to($event->email)->send(new ForgotPasswordMail($event->token));
    }

}

<?php


namespace App\Common;

use Auth;
use Illuminate\Database\Eloquent\Model;

class UserGuard {

    public const EMPLOYER = 'employer';
    public const APPLICANT = 'applicant';

    public static function currentUser($type): ?Model {
        return Auth::guard($type)->user();
    }

}

<?php


namespace App\Common;


class EmailStatus {

    public const EMAIL_CONFIRMED = true;
    public const TOKEN_EXPIRED = null;

}

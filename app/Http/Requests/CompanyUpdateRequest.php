<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:1024|dimensions:max_width=1100,max_height=1100',
            'title' => 'required|string|min:3|max:190',
            'information' => 'required|string|min:3|max:500',
        ];
    }

}

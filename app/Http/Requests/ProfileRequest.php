<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:512',
            'firstname' => 'nullable|string|max:50',
            'lastname' => 'nullable|string|max:50',
            'phone' => 'nullable|string|max:15',
        ];
    }

}

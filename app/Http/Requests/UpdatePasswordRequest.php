<?php

namespace App\Http\Requests;

use App\Rules\EmailExists;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            'email' => ['required', 'email', 'max:190', new EmailExists()],
            'token' => 'required|string|max:50',
            'password' => 'required|string|min:6|max:50|confirmed',
        ];
    }

}

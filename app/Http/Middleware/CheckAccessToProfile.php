<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAccessToProfile {

    public function handle(Request $request, Closure $next, ...$guards) {
        $guards = empty($guards) ? [null] : $guards;

        $urlId = $request->route('id');

        foreach ($guards as $guard) {
            if (isset(Auth::user()->is_admin) || Auth::guard($guard)->id() === $urlId) {
                return $next($request);
            }
        }

        abort(404);
    }

}

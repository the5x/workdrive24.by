<?php

namespace App\Http\Controllers;

use App\Http\Requests\VacancyRequest;
use App\Http\Requests\VacancyRequestUpdate;
use App\Models\Vacancy;
use App\Services\CompanyService;
use App\Services\EmployerService;
use App\Services\FilterService;
use App\Services\OptionService;
use App\Services\VacancyService;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class VacancyController extends Controller {

    private OptionService $optionService;
    private VacancyService $vacancyService;
    private FilterService $filterService;
    private EmployerService $employerService;

    public function __construct(
        OptionService $optionService,
        VacancyService $vacancyService,
        FilterService $filterService,
        EmployerService $employerService
    ) {
        $this->optionService = $optionService;
        $this->vacancyService = $vacancyService;
        $this->filterService = $filterService;
        $this->employerService = $employerService;
    }

    public function index() {
        $options = $this->optionService->all();
        $filteredVacancies = $this->filterService->getVacanciesByQuery();

        return view('vacancies.all', array_merge(['vacancies' => $filteredVacancies], $options));
    }

    public function create(): View {
        $companies = $this->employerService->getAllOwnersCompanies();
        $options = $this->optionService->all();

        return view('vacancies.create', array_merge(['companies' => $companies], $options));
    }

    public function save(VacancyRequest $request): RedirectResponse {
        $vacancy = $this->vacancyService->create($request->validated());
        $this->vacancyService->synchronizeData($vacancy, $request->validated());

        return redirect()->route('vacancies');
    }

    public function edit(string $id): View {
        $vacancy = $this->vacancyService->findById($id);
        $options = $this->optionService->all();

        return view('vacancies.edit', array_merge(['vacancy' => $vacancy], $options));
    }

    public function update(VacancyRequestUpdate $request, string $id): RedirectResponse {
        $vacancy = $this->vacancyService->findById($id);

        $this->vacancyService->update($request->validated(), $id);
        $this->vacancyService->synchronizeData($vacancy, $request->validated());

        return redirect()->route('vacancies');
    }

    public function show(string $id): View {
        $vacancy = $this->vacancyService->findById($id);
        return view('vacancies.show', compact('vacancy'));
    }

    public function delete(string $id): RedirectResponse {
        $vacancy = $this->vacancyService->findById($id);

        $this->vacancyService->delete($id);
        $this->vacancyService->detachData($vacancy);

        return redirect()->route('vacancies');
    }

    public function premium(string $id) {
        Vacancy::where('id', $id)->update(['deadline' => Carbon::now()->addWeek()]);

        return redirect()->back();
    }

    public function free(string $id) {
        Vacancy::where('id', $id)->update(['deadline' => Carbon::now()]);

        return redirect()->back();
    }

}

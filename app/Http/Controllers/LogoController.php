<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use App\Services\LogoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class LogoController extends Controller {

    private LogoService $logoService;
    private CompanyService $companyService;

    public function __construct(
        LogoService $logoService,
        CompanyService $companyService
    ) {
        $this->logoService = $logoService;
        $this->companyService = $companyService;
    }

    public function index() {
        $companies = $this->companyService->all();
        $logos = $this->logoService->all();

        return view('logos.index', compact('companies', 'logos'));
    }

    public function save(Request $request) {
        DB::transaction(function () use ($request) {
            $companies = $request->company;
            $this->logoService->clearLogoTable();

            if (is_array($companies)) {
                foreach ($companies as $company) {
                    $this->logoService->createLogo($company);
                }
            }
        });

        return redirect()->route('logo.index');
    }

}

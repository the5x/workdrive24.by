<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\CarService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CarController extends Controller {

    private CarService $carService;

    public function __construct(
        CarService $carService
    ) {
        $this->carService = $carService;
    }

    public function index(): View {
        $cars = $this->carService->all();
        return view('cars.index', compact('cars'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->carService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->carService->delete($id);
        return redirect()->route('car.index');
    }

}

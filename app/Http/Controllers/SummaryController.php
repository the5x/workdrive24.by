<?php

namespace App\Http\Controllers;

use App\Http\Requests\SummaryRequest;
use App\Services\FilterService;
use App\Services\OptionService;
use App\Services\SummaryService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class SummaryController extends Controller {

    private SummaryService $summaryService;
    private OptionService $optionService;
    private FilterService $filterService;

    public function __construct(
        SummaryService $summaryService,
        OptionService $optionService,
        FilterService $filterService
    ) {
        $this->summaryService = $summaryService;
        $this->optionService = $optionService;
        $this->filterService = $filterService;
    }

    public function index(): View {
        $options = $this->optionService->all();
        $filteredSummaries = $this->filterService->getSummariesByQuery();

        return view('summaries.all', array_merge(['summaries' => $filteredSummaries], $options));
    }

    public function create(): View {
        $options = $this->optionService->all();
        return view('summaries.create', $options);
    }

    public function save(SummaryRequest $request): RedirectResponse {
        $summary = $this->summaryService->create($request->validated());
        $this->summaryService->synchronizeData($summary, $request->validated());

        return redirect()->route('summaries');
    }

    public function edit(string $id): View {
        $options = $this->optionService->all();
        $summary = $this->summaryService->findById($id);

        return view('summaries.edit', array_merge(['summary' => $summary], $options));
    }

    public function update(SummaryRequest $request, string $id): RedirectResponse {
        $summary = $this->summaryService->findById($id);

        $this->summaryService->update($request->validated(), $id);
        $this->summaryService->synchronizeData($summary, $request->validated());

        return redirect()->route('summary.show', ['id' => $id]);
    }

    public function show(string $id): View {
        $summary = $this->summaryService->findById($id);
        return view('summaries.show', compact('summary'));
    }

    public function delete(string $id): RedirectResponse {
        $summary = $this->summaryService->findById($id);

        $this->summaryService->delete($id);
        $this->summaryService->detachData($summary);

        return redirect()->route('summaries');
    }

}

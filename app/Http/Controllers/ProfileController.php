<?php

namespace App\Http\Controllers;

use App\Common\UserGuard;
use App\Http\Requests\ProfileRequest;
use App\Services\ApplicantService;
use App\Services\EmployerService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller {

    private ApplicantService $applicantService;
    private EmployerService $employerService;

    public function __construct(
        ApplicantService $applicantService,
        EmployerService $employerService
    ) {
        $this->applicantService = $applicantService;
        $this->employerService = $employerService;
    }

    public function show(string $id): View {
        $profile = $this->getProfileById($id);

        if (isset($profile)) {
            return view('profile.show', compact('profile'));
        }

        abort(404);
    }

    public function edit(string $id): View {
        $profile = $this->getProfileById($id);

        if ($this->getProfileById($id)) {
            return view('profile.edit', compact('profile'));
        }

        abort(404);
    }

    public function update(ProfileRequest $request, string $id): RedirectResponse {
        if (UserGuard::currentUser(UserGuard::APPLICANT) || isset(Auth::user()->is_admin)) {
            $this->applicantService->update($request->validated(), $id);
        }

        if (UserGuard::currentUser(UserGuard::EMPLOYER) || isset(Auth::user()->is_admin)) {
            $this->employerService->update($request->validated(), $id);
        }

        return redirect()->route('profile.show', ['id' => $id]);
    }

    public function premium(string $id): RedirectResponse {
        $this->employerService->isPremium($id);
        return redirect()->back();
    }

    private function getProfileById(string $id) {
        return $this->applicantService->show($id) ?: $this->employerService->show($id);
    }
}

<?php


namespace App\Http\Controllers\Auth;

use App\Events\ForgotPasswordEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;
use App\Services\PasswordResetService;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ForgotPasswordController extends Controller {

    private PasswordResetService $tokenService;

    public function __construct(
        PasswordResetService $tokenService
    ) {
        $this->tokenService = $tokenService;
    }

    public function index(): View {
        return view('pages.auth.forgot');
    }

    public function sendTokenToEmail(EmailRequest $request): RedirectResponse {
        DB::transaction(function () use ($request) {
            $data = $this->tokenService->saveToken($request->validated());
            event(new ForgotPasswordEvent($data));
        });

        return redirect()->route('login');
    }

}

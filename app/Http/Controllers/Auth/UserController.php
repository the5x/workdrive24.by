<?php


namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller {

    private UserService $userService;

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }

    public function index(): View {
        return view('pages.auth.user');
    }

    public function create(RegisterRequest $request): RedirectResponse {
        DB::transaction(function () use ($request) {
            $user = $this->userService->create($request->validated());
            event(new UserRegisteredEvent($user));
        });

        return redirect()->route('login');
    }

}

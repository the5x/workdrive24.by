<?php


namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Services\ApplicantService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class ApplicantController extends Controller {

    private ApplicantService $applicantService;

    public function __construct(
        ApplicantService $applicantService
    ) {
        $this->applicantService = $applicantService;
    }

    public function index(): View {
        return view('pages.auth.applicant');
    }

    public function create(RegisterRequest $request): RedirectResponse {
        DB::transaction(function () use ($request) {
            $user = $this->applicantService->create($request->validated());
            event(new UserRegisteredEvent($user));
        });

        return redirect()->route('login');
    }

}

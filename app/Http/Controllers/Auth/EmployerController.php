<?php


namespace App\Http\Controllers\Auth;


use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Services\EmployerService;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class EmployerController extends Controller {

    private EmployerService $employerService;
    private const ORDER_BY_EMAIL = 'email';

    public function __construct(
        EmployerService $employerService
    ) {
        $this->employerService = $employerService;
    }

    public function index(): View {
        return view('pages.auth.employer');
    }

    public function create(RegisterRequest $request): RedirectResponse {
        DB::transaction(function () use ($request) {
            $user = $this->employerService->create($request->validated());
            event(new UserRegisteredEvent($user));
        });

        return redirect()->route('login');
    }

    public function all(): View {
        $employers = $this->employerService->all(self::ORDER_BY_EMAIL);
        return view('profile.employers', compact('employers'));
    }

}

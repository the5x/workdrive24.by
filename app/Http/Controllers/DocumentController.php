<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\DocumentService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DocumentController extends Controller {

    private DocumentService $documentService;

    public function __construct(
        DocumentService $documentService
    ) {
        $this->documentService = $documentService;
    }

    public function index(): View {
        $documents = $this->documentService->all();
        return view('documents.index', compact('documents'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->documentService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->documentService->delete($id);
        return redirect()->route('document.index');
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\ResidenceService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ResidenceController extends Controller {

    private ResidenceService $residenceService;

    public function __construct(
        ResidenceService $residenceService
    ) {
        $this->residenceService = $residenceService;
    }

    public function index(): View {
        $residences = $this->residenceService->all();
        return view('residences.index', compact('residences'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->residenceService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->residenceService->delete($id);
        return redirect()->route('residence.index');
    }

}

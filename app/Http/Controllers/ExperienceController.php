<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\ExperienceService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ExperienceController extends Controller {

    private ExperienceService $experienceService;

    public function __construct(
        ExperienceService $experienceService
    ) {
        $this->experienceService = $experienceService;
    }

    public function index(): View {
        $experiences = $this->experienceService->all();
        return view('experiences.index', compact('experiences'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->experienceService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->experienceService->delete($id);
        return redirect()->route('experience.index');
    }

}

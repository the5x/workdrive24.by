<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\LicenseService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class LicenseController extends Controller {

    private LicenseService $licenseService;

    public function __construct(
        LicenseService $licenceService
    ) {
        $this->licenseService = $licenceService;
    }

    public function index(): View {
        $licenses = $this->licenseService->all();
        return view('license.index', compact('licenses'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->licenseService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->licenseService->delete($id);
        return redirect()->route('license.index');
    }

}

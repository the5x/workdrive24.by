<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use App\Services\LogoService;
use App\Services\SummaryService;
use App\Services\VacancyService;
use Illuminate\Contracts\View\View;

class HomeController extends Controller {

    private const LIMIT_SUMMARIES = 4;
    private const LIMIT_VACANCIES = 4;
    private SummaryService $summaryService;
    private VacancyService $vacancyService;
    private CompanyService $companyService;
    private LogoService $logoService;

    public function __construct(
        SummaryService $summaryService,
        VacancyService $vacancyService,
        CompanyService $companyService,
        LogoService $logoService
    ) {
        $this->summaryService = $summaryService;
        $this->vacancyService = $vacancyService;
        $this->companyService = $companyService;
        $this->logoService = $logoService;
    }

    public function index(): View {
        $countSummaries = count($this->summaryService->all());
        $countVacancies = count($this->vacancyService->all());
        $countCompanies = count($this->companyService->all());

        $logos = $this->logoService->all();
        $vacancies = $this->vacancyService->takeLimitVacancies(self::LIMIT_VACANCIES);
        $summaries = $this->summaryService->takeLimitAmount(self::LIMIT_SUMMARIES);

        return view('pages.welcome',
            compact('summaries', 'countSummaries', 'countVacancies', 'countCompanies', 'vacancies', 'logos')
        );
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Mail\CompanyCreated;
use App\Services\CompanyService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller {

    private const ADMIN_EMAIL = 'client@workdrive24.by';
    private CompanyService $companyService;

    public function __construct(
        CompanyService $companyService
    ) {
        $this->companyService = $companyService;
    }

    public function create(): View {
        return view('companies.create');
    }

    public function save(CompanyRequest $request): RedirectResponse {
        $company = $this->companyService->create($request->validated());
        Mail::to(self::ADMIN_EMAIL)->send(new CompanyCreated($company));
        return redirect()->route('home');
    }

    public function edit(string $id): View {
        $company = $this->companyService->findById($id);
        return view('companies.edit', compact('company'));
    }

    public function update(CompanyUpdateRequest $request, string $id): RedirectResponse {
        $this->companyService->update($request->validated(), $id);
        return redirect()->route('company.show', ['id' => $id]);
    }

    public function show(string $id): View {
        $company = $this->companyService->findById($id);
        return view('companies.show', compact('company'));
    }

    public function all(): View {
        $companies = $this->companyService->all();
        return view('companies.all', compact('companies'));
    }

    public function delete(string $id): RedirectResponse {
        $this->companyService->delete($id);
        return redirect()->route('company.all');
    }

}

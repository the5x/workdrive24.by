@extends('layouts.admin')

@section('content')


    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home') }}">Главная</a>
            <h3 class="section__title section__title_sm">Обновить вакансию</h3>

            <form action="{{ route('vacancy.update', ['id' => $vacancy->id]) }}" method="POST">
                @method('PUT')
                @csrf

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('title')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">@lang('summary.title')</label>
                        <input class="admin__input" type="text" name="title"
                               value="{{ old('title') ?: $vacancy->title }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('start_salary')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="start_salary" class="admin__label">@lang('summary.salary_from')</label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="start_salary" value="{{ old('start_salary') ?: $vacancy->start_salary }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('end_salary')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="end_salary" class="admin__label">@lang('summary.salary_to')</label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="end_salary" value="{{ old('end_salary') ?: $vacancy->end_salary }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('company')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="company" class="admin__label">@lang('summary.company')</label>
                        <h4 class="filter__option-subtitle">{{ $vacancy->company->title }}</h4>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <div class="filter__option">

                            @error('license')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.driver_license')</h4>
                            @foreach($licenses as $license)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $license->id }}"
                                        {{ $vacancy->licenses->contains($license) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $license->id }}"
                                        name="license[]">
                                    <label for="{{ $license->id }}"
                                           class="filter__option-title">{{ $license->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">

                            @error('documentation')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.additional_document')</h4>

                            @foreach($documentations as $documentation)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $documentation->id }}"
                                        {{ $vacancy->documentations->contains($documentation) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $documentation->id }}"
                                        name="documentation[]">
                                    <label for="{{ $documentation->id  }}"
                                           class="filter__option-title">{{ $documentation->title  }}</label>
                                </div>
                            @endforeach

                        </div>

                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('residence')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.place_residence')</h4>

                            @foreach($residences as $residence)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $residence->id }}"
                                        {{ $vacancy->residences->contains($residence) ? 'checked' : '' }}
                                        class="filter__option-ui" type="checkbox"
                                        value="{{ $residence->id }}"
                                        name="residence[]">
                                    <label for="{{ $residence->id }}"
                                           class="filter__option-title">{{ $residence->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('language')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.language')</h4>

                            @foreach($languages as $language)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $language->id }}"
                                        {{ $vacancy->languages->contains($language) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $language->id }}"
                                        name="language[]">
                                    <label for="{{ $language->id }}"
                                           class="filter__option-title">{{ $language->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <div class="filter__option">
                            @error('car')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.car_type')</h4>

                            @foreach($cars as $car)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $car->id }}"
                                        {{ $vacancy->cars->contains($car) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $car->id }}"
                                        name="car[]">
                                    <label for="{{ $car->id }}"
                                           class="filter__option-title">{{ $car->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('experience')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.work_experience')</h4>

                            @foreach($experiences as $experience)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $experience->id }}"
                                        {{ $vacancy->experiences->contains($experience) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="radio" value="{{ $experience->id }}"
                                        name="experience">
                                    <label for="{{ $experience->id }}"
                                           class="filter__option-title">{{ $experience->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('internship')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.internship')</h4>

                            @foreach($internships as $internship)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $internship->id }}"
                                        {{ $vacancy->internships->contains($internship) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="radio" value="{{ $internship->id }}"
                                        name="internship">
                                    <label for="{{ $internship->id }}"
                                           class="filter__option-title">{{ $internship->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('document')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">@lang('summary.help_with_document')</h4>

                            @foreach($documents as $document)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $document->id }}"
                                        {{ $vacancy->documents->contains($document) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="radio" value="{{ $document->id }}"
                                        name="document">
                                    <label for="{{ $document->id }}"
                                           class="filter__option-title">{{ $document->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('information')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">@lang('summary.additional_data')</h4>
                        <textarea class="admin__textarea" name="information" id="" cols="30"
                                  rows="10">{{ old('information') ?: $vacancy->information }}</textarea>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">

                        @error('is_direct_employer')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">@lang('summary.direct_employer')</h4>

                        <div class="admin__primary">
                            <input id="is_direct_employer" onclick="chooseDirectEmployer();" value="{{ $vacancy->is_direct_employer }}" {{ $vacancy->is_direct_employer ? 'checked' : '' }} class="admin__input admin__input_inline js_is_direct_employer" type="checkbox" name="is_direct_employer" />

                            <label for="is_direct_employer" class="admin__label admin__label_inline">@lang('summary.direct_employer')</label>
                        </div>

                        <script>

                            function chooseDirectEmployer() {
                                const SELECTED = 1;
                                const NOT_SELECTED = 0;

                                const directEmployerElement = document.querySelector('.js_is_direct_employer');

                                directEmployerElement.checked ?
                                    directEmployerElement.value = SELECTED :
                                    directEmployerElement.value = NOT_SELECTED;
                            }

                        </script>
                    </div>
                </div>

                <button class="filter__button" type="submit">@lang('summary.update_vacancy')</button>
            </form>

            <form action="{{ route('vacancy.delete', ['id' => $vacancy->id]) }}" method="POST">
                @method('DELETE')
                @csrf

                <button class="simple__button" type="submit">@lang('summary.delete_vacancy')</button>
            </form>
        </section>
    </div>
@stop

@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home') }}">Главная</a>

            <div class="necessarily">
                <strong class="necessarily__subtitle">@lang('summary.necessarily_item_1')</strong>
                <p class="necessarily__text">1. @lang('summary.necessarily_item_2') <a href="{{ route('vacancy.create') }}">@lang('summary.necessarily_item_2_link')</a></p>
                <p class="necessarily__text">2. @lang('summary.necessarily_item_3')</p>
                <p class="necessarily__text">3. @lang('summary.necessarily_item_4')</p>
            </div>

            <h3 class="section__title section__title_sm">@lang('summary.create_vacancy')</h3>

            <form action="{{ route('vacancy.create') }}" method="POST">
                @csrf

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('title')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">@lang('summary.title')</label>
                        <input class="admin__input" type="text" name="title" value="{{ old('title') }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('company')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="company" class="admin__label">@lang('summary.company')</label>
                        <select class="admin__input" name="company" id="company">
                            @foreach($companies as $company)
                                <option {{ old('company') === $company->id ? 'selected' : '' }} value="{{ $company->id }}">{{ $company->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('start_salary')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="start_salary" class="admin__label">@lang('summary.salary_from'):</label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="start_salary" value="{{ old('start_salary') }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('end_salary')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="end_salary" class="admin__label">@lang('summary.salary_to'):</label>
                        <input class="admin__input admin__input_mw20" type="number"
                               name="end_salary" value="{{ old('end_salary') }}"/>
                    </div>
                </div>

                @include('includes.summary-vacancy-options-when-create')

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('information')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">@lang('summary.additional_data')</h4>
                        <textarea class="admin__textarea" name="information" cols="30"
                                  rows="10">{{ old('information') }}</textarea>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">

                        @error('is_direct_employer')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">@lang('summary.direct_employer')</h4>

                        <div class="admin__primary">
                            <input id="is_direct_employer" onclick="chooseDirectEmployer();" value="0" class="admin__input admin__input_inline js_is_direct_employer" type="checkbox" name="is_direct_employer" />

                            <label for="is_direct_employer" class="admin__label admin__label_inline">@lang('summary.direct_employer')</label>
                        </div>

                        <script>

                            function chooseDirectEmployer() {
                                const SELECTED = 1;
                                const NOT_SELECTED = 0;

                                const directEmployerElement = document.querySelector('.js_is_direct_employer');

                                directEmployerElement.checked ?
                                    directEmployerElement.value = SELECTED :
                                    directEmployerElement.value = NOT_SELECTED;
                            }

                        </script>

                    </div>
                </div>

                <button class="filter__button" type="submit">@lang('summary.create_vacancy')</button>
            </form>
        </section>
    </div>
@stop

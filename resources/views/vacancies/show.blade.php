@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('vacancies')  }}">На страницу вакансий</a>
        <section class="description">
            <h3 class="section__title section__title_sm">
                {{ $vacancy->title }}

                @if(Auth::id() === $vacancy->user_id || isset(Auth::user()->is_admin))
                    <a class="link__ico" href="{{ route('vacancy.edit', ['id' => $vacancy->id]) }}">
                        <img class="link__ico-pic" src="{{ URL::asset('assets/images/ico-edit.svg') }}" alt=""/>
                    </a>
                @endif
            </h3>

            <div class="price__column">
                @include('includes.start-end-salary')
            </div>

            <br>

            @if(isset(Auth::user()->is_admin))

                @if(\Carbon\Carbon::now() < $vacancy->deadline)
                    <form action="{{ route('vacancy.free', ['id' => $vacancy->id]) }}" method="post">
                        @method('PUT')
                        @csrf

                        <button class="btn__premium">
                            <img class="btn__premium-ico btn__premium-ico_opacity"
                                 src="{{ URL::asset('assets/images/ico-premium.svg') }}" alt=""/>
                            <span class="btn__premium-text btn__premium-text_opacity">Снять выделение с вакансии</span>
                        </button>
                    </form>
                @else
                    <form action="{{ route('vacancy.premium', ['id' => $vacancy->id]) }}" method="post">
                        @method('PUT')
                        @csrf

                        <button class="btn__premium">
                            <img class="btn__premium-ico"
                                 src="{{ URL::asset('assets/images/ico-premium.svg') }}" alt=""/>
                            <span class="btn__premium-text">Выделить вакансию на 1 неделю</span>
                        </button>
                    </form>
                @endif
            @endif

            <br>

            <div class="description__grid">
                <div class="description__column">
                    <div class="description__company">

                        <div class="description__profile-cover">
                            @if(isset($vacancy->company->photo) && !empty($vacancy->company->photo))
                                <img class="profile__pic" src="{{ asset('uploads/' . $vacancy->company->photo )}}"
                                     alt="">
                            @else
                                <div class="profile__column profile__column_sm profile__column_border"></div>
                            @endif
                        </div>

                        @if($vacancy->is_direct_employer)
                        <div class="description__badge">
                            @lang('summary.direct_employer')
                        </div>
                        @endif

                        <div class="description__company-column">
                            <span class="descripktion__company-name">
                                <a class="vacancy__meta-link"
                                   href="{{ route('company.show', ['id' => $vacancy->company->id]) }}">{{ $vacancy->company->title }}</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="description__column description__column_fl2">
                    <h5 class="description__subtitle">Требования</h5>
                    <ul class="description__list">
                        <li class="description__item">
                            Удостоверение:
                            <br/>
                            @foreach($vacancy->licenses as $licence)
                                <span class="description__item-option">{{ $licence->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Дополнительные документы:
                            <br/>
                            @foreach($vacancy->documentations as $documentation)
                                <span class="description__item-option">{{ $documentation->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Место жительства:
                            <br/>
                            @foreach($vacancy->residences as $residence)
                                <span class="description__item-option">{{ $residence->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Тип авто:
                            <br/>
                            @foreach($vacancy->cars as $car)
                                <span class="description__item-option">{{ $car->title  }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Стажировка:
                            <br/>
                            @foreach($vacancy->internships as $internship)
                                <span class="description__item-option">{{ $internship->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Языки:
                            <br/>
                            @foreach($vacancy->languages as $language)
                                <span class="description__item-option">{{ $language->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Помощь с документами:
                            <br/>
                            @foreach($vacancy->documents as $document)
                                <span class="description__item-option">{{ $document->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            Опыт работы:
                            <br/>
                            @foreach($vacancy->experiences as $experience)
                                <span class="description__item-option">{{ $experience->title }}</span>
                            @endforeach
                        </li>
                    </ul>
                </div>
                <div class="description__column">
                    <div class="description__user">
                        <strong class="description__subtitle">Контактное лицо</strong>
                        @if(isset($vacancy->employer->firstname, $vacancy->employer->lastname))
                            <span class="description__user-text">
                                {{ $vacancy->employer->firstname }} {{ $vacancy->employer->lastname }}
                            </span>
                        @else
                            <em class="description__user-text">Нет данных</em>
                        @endif

                        <strong class="description__subtitle">Номер телефона</strong>

                        @if(isset( $vacancy->employer->phone ))
                            <a href="tel:{{ $vacancy->employer->phone }}"
                               class="description__user-text description__user-link">
                                {{ $vacancy->employer->phone }}
                            </a>
                        @else
                            <em class="description__user-text">Нет данных</em>
                        @endif

                        <strong class="description__subtitle">Электронная почта</strong>
                        <a href="mailto:{{ $vacancy->employer->email }}"
                           class="description__user-text description__user-text_mb description__user-link">{{ $vacancy->employer->email }}</a>
                    </div>
                </div>
            </div>

            <div class="description__other">
                <h3 class="description__subtitle">Дополнительные данные</h3>
                <p class="description__text">{{ $vacancy->information }}</p>
            </div>

        </section>
    </div>

@stop

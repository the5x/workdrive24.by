@extends('layouts.child')

@section('title', 'Свежие вакансии водителя по всей Беларуси — быстрый подбор | WORKDRIVE24')
@section('description', 'Свежие вакансии водителя по всей Беларуси с любой категорией от сервиса WORKDRIVE24. Регистрируйтесь на нашем сайте, размещайте резюме и следите за новыми вакансиями от различных компаний!')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <div class="cascade__grid">
            <div class="cascade__column cascade__column_mw">
                <div class="filter">
                    <h3 class="section__title section__title_sm">@lang('summary.selection')</h3>
                    <form action="{{ route('vacancies') }}" method="GET">
                        @include('includes.filter')
                        <button class="filter__button" type="submit">@lang('summary.find_vacancies')</button>

                        @if( array_key_exists('query', parse_url( Request::fullUrl() )) )
                            <a href="{{ route('vacancies') }}" class="filter__reset">@lang('summary.reset')</a>
                        @else
                            <button class="filter__reset" type="reset">@lang('summary.reset')</button>
                        @endif
                    </form>
                </div>
            </div>
            <div class="cascade__column cascade__column_ml">
                <h1 class="section__title section__title_sm">@lang('summary.vacancies')</h1>

                @foreach($vacancies as $vacancy)
                    <div
                        class="summary__item summary__item_full {{ \Carbon\Carbon::now() < $vacancy->deadline ? 'summary__item_yellow' : '' }}">
                        @include('includes.vacancy_small')
                    </div>
                @endforeach

                {{ $vacancies->appends(request()->query())->onEachSide(1)->links() }}

                <h2 class="about__subtitle">Подробнее о наших вакансиях</h2>
                <p class="about__info">Водитель международных перевозок - эта профессия по праву считается одной из элитных среди водителей. Расстояние, которое эти ребята наматывают всего за год работы, наверное, мы не проходим и за всю жизнь. В любых погодных условиях они продолжают свой путь, останавливаясь лишь при погрузках и выгрузках товара.</p>
                <p class="about__info">Где и как найти свежие вакансии водителя, рассказываем ниже.</p>
                <h2 class="about__subtitle">Работа водителя международника вакансии легко найти на нашем сайте</h2>
                <p class="about__info">Чтобы поиск работы не занимал у вас много времени, а все компании, которые находятся в поисках водителя были перед вами, был создан сервис WORKDRIVE.</p>
                <p class="about__info">На сайте компании вам необходимо разместить свое резюме (пройдя перед этим регистрацию) и регулярно просматривать вакансии, которые появляются ежедневно.</p>
                <p class="about__info">Также работодатель может сам подобрать себе сотрудников: для этого необходимо пройти процедуру регистрации, просмотреть резюме водителей и связаться с теми, чья квалификация и опыт удовлетворит ваши потребности.</p>
                <h2 class="about__subtitle">Работа дальнобойщиком: вакансии есть всегда</h2>
                <p class="about__info">Наличие большого количества вакансий вызвано дефицитом квалифицированных водителей, которые работают на большегрузных автомобилях. Многие компании рассматривают вакансии водителей даже с небольшим опытом работы и просто «вводят их в курс дела»</p>
                <p class="about__info">На нашем сайте вы сможете найти вакансии водителя без опыта работы. Будьте уверены, что они тоже просматриваются работодателями и находят свой отклик, ведь многие компании предоставляют корпоративное обучение. Все будет зависеть от вашего желания и стремления.</p>
                <h2 class="about__subtitle">Работа водителем прямые вакансии ищите на нашем сайте</h2>
                <p class="about__info">Искать работу через знакомых или посредников не есть хорошо: ведь вас могут не просто дезинформировать, но и использовать ваши личные данные в корыстных целях.</p>
                <p class="about__info">Чтобы избежать этих неприятных моментов, WORKDRIVE предпочитает работать с компаниями напрямую. Работодатель может найти вас и связаться только после прохождения регистрации. Не забывайте и вы регулярно просматривать вакансии и откликаться, если работа соответствует вашим ожиданиям и требованиям.</p>
                <p class="about__info">Workdrive24 — ваш старт к успеху!</p>
            </div>
        </div>

    </div>
@stop

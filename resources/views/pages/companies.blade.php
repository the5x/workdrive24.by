@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <h3 class="section__title section__title_sm">Компании</h3>
        <div class="partners__list">
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
            <div class="partners__item">
                <a class="partners__item-link" href="#">
                    <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>
@stop

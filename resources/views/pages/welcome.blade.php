@extends('layouts.app')

@section('title', 'Работа водителем — свежие вакансии по всей Беларуси | WORKDRIVE24')
@section('description', 'Работа водителем — свежие вакансии в Беларуси для водителей любой категории: B, C, D, E. WORKDRIVE24 — уникальный сервис, который позволяет следить за вакансиями водителей, водителей-международников от различных компаний, а прямым работодателям размещать актуальные вакансии. Регистрируйтесь!')

@section('hero')
    <div class="hero__box">
        <div class="hero__slider">
            @auth('applicant')
                <a href="{{ route('summary.create') }}" class="hero__link hero__link_yellow">@lang('slider.create_cv')</a>
            @endauth
            @auth('employer')
                <a href="{{ route('company.create') }}" class="hero__link hero__link_yellow">@lang('slider.create_company')</a>
                <a href="{{ route('vacancy.create') }}" class="hero__link hero__link_purple">@lang('slider.create_vacancy')</a>
            @endauth
            <h1 class="hero__title">@lang('slider.title')</h1>
        </div>

        <div class="statistics">
            <div class="statistics__column statistics__column_summary">
                <strong class="statistics__title">{{ $countSummaries }}</strong>
                <span class="statistics__subtitle">@lang('slider.summaries')</span>
            </div>
            <div class="statistics__column statistics__column_job">
                <strong class="statistics__title">{{ $countVacancies }}</strong>
                <span class="statistics__subtitle">@lang('slider.vacancies')</span>
            </div>
            <div class="statistics__column statistics__column_company">
                <strong class="statistics__title">{{ $countCompanies }}</strong>
                <span class="statistics__subtitle statistics__subtitle_mb">@lang('slider.companies')</span>
            </div>
        </div>
    </div>

@stop


@section('vacancy')
    <div class="vacancy__list">
        @foreach($vacancies as $vacancy)
            @include('includes.vacancy')
        @endforeach
    </div>
@stop


@section('summary')
    <div class="summary__list">
        @foreach($summaries as $summary)
            <div class="summary__item">
                @include('includes.summary')
            </div>
        @endforeach
    </div>
@stop

@section('partners')

    <div class="partners__list">
        @if(empty($logos))

        @else
            @foreach($logos as $logo)
                <div class="partners__item">
                    <div class="partners__cover" style="background-image: url({{ asset('uploads/' .  $logo->company->photo) }})"></div>
                    <a class="partners__item-link" href="{{ route('company.show', ['id' => $logo->company->id])  }}">{{ $logo->company->title }}</a>
                </div>
            @endforeach
        @endif
    </div>

@stop


@section('about')
    <h3 class="section__title">@lang('title.about_us')</h3>
    <div class="about__fragment">
        <p class='about__info'>Водитель — это специалист, который отвечает за управление транспортным средством, а также доставку грузов и пассажиров. Увеличение количества транспортных средств «закрепило» работу водителя на первых позициях по востребованности. Не знаете, где найти работу водителем международником с достойной оплатой труда? WORKDRIVE вам в помощь!</p>

        <span onclick="openDescription()" class="more">Читать полностью...</span>
    </div>
    <div class="about__full">
        <h2 class='about__subtitle'>Хотите найти работу водителем в Минске, Гродно, Гомеле и других городах Беларуси? Считайте, что уже нашли</h2>
        <p class='about__info'>Что такое сервис WORKDRIVE? Это то самое место, где вы сможете найти высокооплачиваемую работу или сотрудников с категорией прав B, C, D, E. Работа водителем свежие вакансии и резюме появляются на сайте ежедневно, что еще раз подтверждает спрос на нее.</p>
        <h2 class='about__subtitle'>Почему работа водителем в Беларуси всегда востребована?</h2>
        <p class='about__info'>Несмотря на наличие некоторых минусов (сидячий образ жизни, ненормированный рабочий день и т.д.), работа имеет и множество положительных сторон, например: </p>
        <ul class='about__list'>
            <li><p class='about__info'>Хоть и проездом, но вы сможете увидеть многие города и страны;</p></li>
            <li><p class='about__info'>Не нужно учиться годами, чтобы получить водительское  удостоверение - всего пара недель - месяцев обучения и наличие опыта вождения помогут вам получить заветную должность;</p></li>
            <li><p class='about__info'>Водители обладают отличной памятью, ведь им приходится запоминать адреса и изучать местность;</p></li>
        </ul>
        <h2 class='about__subtitle'>Работа водителем от прямых работодателей безопасное решение?</h2>
        <p class='about__info'>Поддерживая связь напрямую с работодателем, вы обезопасите себя, свою личную информацию, не нарвавшись на мошенников. Как это возможно? Компания WORKDRIVE приняла для этого всего меры: сделала регистрацию платной, объявления работодателей проходят проверку и лишь после этого появляются в разделе вакансий.</p>
        <p class='about__info'>Обратите внимание: как резюме, так и вакансию можно разместить только после регистрации — это еще одна мера безопасности нашего сервиса.</p>
        <h2 class='about__subtitle'>Мечтаете о работе водителем международником? С нами ваши мечты станут реальностью!</h2>
        <p class='about__info'>Работа водителем дальнобойщиком считается высокооплачиваемой не только в Беларуси, но и во всем мире.</p>
        <p class='about__info'>Все еще думаете, что не сможете найти вакансию? Это доступно и реально, WORKDRIVE в этом поможет! Регистрируйтесь на нашем сайте, размещайте резюме и следите за новыми вакансиями от различных компаний. Уверены, долго ждать не придется.</p>
        <p class='about__info'>Если вы являетесь работодателем, регистрируйтесь на нашем сайте, размещайте вакансию и получайте возможность самостоятельно отобрать подходящие резюме, не дожидаясь отклика кандидатов.</p>
        <p class='about__info'>Workdrive24 — ваш старт к успеху!</p>
    </div>

    <script>
        function openDescription() {
            const moreLinkElem = document.querySelector('.more');
            moreLinkElem.style.display = 'none';

            const blockDescription = document.querySelector('.about__full');
            blockDescription.style.display = 'block';
        }
    </script>
@stop

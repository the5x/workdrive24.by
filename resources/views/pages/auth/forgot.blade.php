@extends('layouts.auth')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>

        <section class="auth">
            <h3 class="section__title section__title_sm">@lang('auth.recover_password')</h3>
            <br /><br />
            <form class="auth__form" action="{{ route('forgot.send') }}" method="post">
                @csrf
                <div class="auth__ui">
                    <div class="auth__ui-column">
                        @error('email')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="email" class="auth__ui-label">Email</label>
                        <input class="auth__ui-field" type="email" name="email" value="{{ old('email') }}">
                    </div>
                    <button class="filter__button">@lang('auth.send')</button>
                </div>
            </form>

            <a class="auth__forgot-link" href="{{ route('login')  }}">@lang('auth.login')</a>

            <div class="auth__choose">
                <a class="auth__choose-link auth__choose-link_yellow" href="{{ route('applicant.index') }}">@lang('auth.registration_as_applicant')</a>
                <a class="auth__choose-link auth__choose-link_purple" href="{{ route('employer.index') }}">@lang('auth.registration_as_employer')</a>
            </div>
        </section>
    </div>
@stop

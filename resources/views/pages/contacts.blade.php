@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>

        <section class="company">
            <div class="company__grid company__grid_mt-mb">
                <div class="company__column">
                    <p class="company__text">
                        @lang('contact.contact_a_description')
                    </p>
                </div>
                <div class="company__column">
                    <a class="company__link" href="https://wa.me/+375445522424">+375 44 552-24-24</a>
                    <small class="company__text company__text_grey">Whatsapp</small>
                </div>
                <div class="company__column">
                    <a class="company__link" href="https://telegram.me/+375445522424">+375 44 552-24-24</a>
                    <small class="company__text company__text_grey">Telegram</small>
                </div>
                <div class="company__column">
                    <a class="company__link" href="viber://contact?number=%2B+375445522424">+375 44 552-24-24</a>
                    <small class="company__text company__text_grey">Viber</small>
                </div>
            </div>

            <div class="company__grid">
                <div class="company__column">
                    <p class="company__text">
                        @lang('contact.contact_b_description')
                    </p>
                </div>
                <div class="company__column company__column_fl3">
                    <a class="company__email" href="mailto:client@workdrive24.by">client@workdrive24.by</a>
                </div>
            </div>

            <div class="company__grid company__grid_mt-mb">
                <div class="company__column">
                    <p class="company__text">
                        @lang('contact.follow_us')
                    </p>
                </div>
                <div class="company__column">
                    <a target="_blank" href="https://instagram.com/workdrive24?utm_medium=copy_link"><img src="{{ URL::asset('assets/images/ico-insta.png') }}" alt=""></a>
                </div>
                <div class="company__column">
                    <a target="_blank" href="https://m.facebook.com/Workdrive24/?ref=bookmarks"><img src="{{ URL::asset('assets/images/ico-facebook.png') }}" alt=""></a>
                </div>
                <div class="company__column"></div>
            </div>
        </section>
    </div>
@stop

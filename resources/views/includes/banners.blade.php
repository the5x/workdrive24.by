@php

    $banners = [
        ['link' => 'https://workdrive24.by/companies/88e7b252-5c21-4f5f-9699-5c5fa57738d8', 'image'=> 'personal_center.png'],
    ];

    $randomBanner = empty($banners) ? [] : $banners[array_rand($banners, 1)];
@endphp

@if(empty($randomBanner))
    <a href="{{ route('rates') }}">
        <div class="rate__banner"></div>
    </a>
@else
    <a target="_blank" class="banner__link" href="{{ $randomBanner['link'] }}">
        <img class="banner__link-images" src="{{ URL::asset('assets/banners/' . $randomBanner['image']) }}" alt="">
    </a>
@endempty

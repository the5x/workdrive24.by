<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.driver_license')</h4>

    @foreach($licenses as $license)
        <div class="filter__option-column">
            <input
                {{ !empty(request('license')) && in_array($license->id, request('license')) ? 'checked' : '' }}
                id="{{ $license->id }}" class="filter__option-ui" type="checkbox" value="{{ $license->id }}"
                name="license[]">
            <label for="{{ $license->id }}"
                   class="filter__option-title">{{ $license->title }}</label>
        </div>
    @endforeach

</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.additional_document')</h4>

    @foreach($documentations  as $documentation)
        <div class="filter__option-column">
            <input
                {{ !empty(request('documentation')) && in_array($documentation->id, request('documentation')) ? 'checked' : '' }}
                id="{{ $documentation->id }}" class="filter__option-ui" type="checkbox" value="{{ $documentation->id }}"
                name="documentation[]">
            <label for="{{ $documentation->id }}"
                   class="filter__option-title">{{ $documentation->title  }}</label>
        </div>
    @endforeach
</div>


<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.place_residence')</h4>

    @foreach($residences as $residence)
        <div class="filter__option-column">
            <input
                {{ !empty(request('residence')) && in_array($residence->id, request('residence')) ? 'checked' : '' }}
                id="{{ $residence->id }}" class="filter__option-ui" type="checkbox" value="{{ $residence->id }}"
                name="residence[]">
            <label for="{{ $residence->id }}"
                   class="filter__option-title">{{ $residence->title }}</label>
        </div>
    @endforeach
</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.language')</h4>

    @foreach($languages as $language)
        <div class="filter__option-column">
            <input
                {{ !empty(request('language')) && in_array($language->id, request('language')) ? 'checked' : '' }}
                id="{{ $language->id }}" class="filter__option-ui" type="checkbox" value="{{ $language->id }}"
                name="language[]">
            <label for="{{ $language->id }}"
                   class="filter__option-title">{{ $language->title }}</label>
        </div>
    @endforeach
</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.car_type')</h4>

    @foreach($cars as $car)
        <div class="filter__option-column">
            <input
                {{ !empty(request('car')) && in_array($car->id, request('car')) ? 'checked' : '' }}
                id="{{ $car->id }}" class="filter__option-ui" type="checkbox" value="{{ $car->id }}"
                name="car[]">
            <label for="{{ $car->id }}"
                   class="filter__option-title">{{ $car->title }}</label>
        </div>
    @endforeach
</div>


<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.work_experience')</h4>

    @foreach($experiences as $experience)
        <div class="filter__option-column">
            <input
                {{ !empty(request('experience')) && $experience->id === request('experience') ? 'checked' : '' }}
                id="{{ $experience->id }}" class="filter__option-ui" type="radio" value="{{ $experience->id }}"
                name="experience">
            <label for="{{ $experience->id }}"
                   class="filter__option-title">{{ $experience->title }}</label>
        </div>
    @endforeach
</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.internship')</h4>

    @foreach($internships as $internship)
        <div class="filter__option-column">
            <input
                {{ !empty(request('internship')) && $internship->id === request('internship') ? 'checked' : '' }}
                id="{{ $internship->id }}" class="filter__option-ui" type="radio" value="{{ $internship->id }}"
                name="internship">
            <label for="{{ $internship->id }}"
                   class="filter__option-title">{{ $internship->title }}</label>
        </div>
    @endforeach

</div>

<div class="filter__option">
    <h4 class="filter__option-subtitle">@lang('summary.help_with_document')</h4>

    @foreach($documents as $document)
        <div class="filter__option-column">
            <input
                {{ !empty(request('document')) && $document->id === request('document') ? 'checked' : '' }}
                id="{{ $document->id }}" class="filter__option-ui" type="radio" value="{{ $document->id }}"
                name="document">
            <label for="{{ $document->id }}"
                   class="filter__option-title">{{ $document->title }}</label>
        </div>
    @endforeach
</div>

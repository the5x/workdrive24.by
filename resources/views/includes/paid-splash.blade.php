<div class="attention">
    <h4 class="attention__title">@lang('summary.splash_title')</h4>
    <p class="attention__text">@lang('summary.message')
        <a class="attention__link" href="{{ route('rates') }}">→</a></p>
</div>

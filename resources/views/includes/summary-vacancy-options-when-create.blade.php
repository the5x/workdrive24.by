<div class="admin__grid">
    <div class="admin__column">
        <div class="filter__option">

            @error('license')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.driver_license')</h4>

            @foreach($licenses as $license)
                <div class="filter__option-column">
                    <input
                        {{ is_array(old('license')) && in_array($license->id, old('license')) ? 'checked ' : '' }}
                        id="{{ $license->id }}" class="filter__option-ui" type="checkbox"
                        value="{{ $license->id }}"
                        name="license[]">
                    <label for="{{ $license->id }}"
                           class="filter__option-title">{{ $license->title }}</label>
                </div>
            @endforeach

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">

            @error('documentation')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.additional_document')</h4>

            @foreach($documentations  as $documentation)
                <div class="filter__option-column">
                    <input
                        {{ is_array(old('documentation')) && in_array($documentation->id, old('documentation')) ? 'checked ' : '' }}
                        id="{{ $documentation->id }}"
                        class="filter__option-ui" type="checkbox" value="{{ $documentation->id }}"
                        name="documentation[]">
                    <label for="{{ $documentation->id  }}"
                           class="filter__option-title">{{ $documentation->title  }}</label>
                </div>
            @endforeach

        </div>

    </div>

    <div class="admin__column">
        <div class="filter__option">
            @error('residence')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.place_residence')</h4>

            @foreach($residences as $residence)
                <div class="filter__option-column">
                    <input
                        {{ is_array(old('residence')) && in_array($residence->id, old('residence')) ? 'checked ' : '' }}
                        id="{{ $residence->id }}"
                        class="filter__option-ui" type="checkbox" value="{{ $residence->id }}"
                        name="residence[]">
                    <label for="{{ $residence->id }}"
                           class="filter__option-title">{{ $residence->title }}</label>
                </div>
            @endforeach

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            @error('language')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.language')</h4>

            @foreach($languages as $language)
                <div class="filter__option-column">
                    <input
                        {{ is_array(old('language')) && in_array($language->id, old('language')) ? 'checked ' : '' }}
                        id="{{ $language->id }}"
                        class="filter__option-ui" type="checkbox" value="{{ $language->id }}"
                        name="language[]">
                    <label for="{{ $language->id }}"
                           class="filter__option-title">{{ $language->title }}</label>
                </div>
            @endforeach

        </div>
    </div>
</div>

<div class="admin__grid">
    <div class="admin__column">
        <div class="filter__option">
            @error('car')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.car_type')</h4>

            @foreach($cars as $car)
                <div class="filter__option-column">
                    <input
                        {{ is_array(old('car')) && in_array($car->id, old('car')) ? 'checked ' : '' }}
                        id="{{ $car->id }}"
                        class="filter__option-ui" type="checkbox" value="{{ $car->id }}"
                        name="car[]">
                    <label for="{{ $car->id }}"
                           class="filter__option-title">{{ $car->title }}</label>
                </div>
            @endforeach

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            @error('experience')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.work_experience')</h4>

            @foreach($experiences as $experience)
                <div class="filter__option-column">
                    <input
                        {{ old('experience') === $experience->id ? 'checked' : '' }}
                        id="{{ $experience->id }}"
                        class="filter__option-ui" type="radio" value="{{ $experience->id }}"
                        name="experience">
                    <label for="{{ $experience->id }}"
                           class="filter__option-title">{{ $experience->title }}</label>
                </div>
            @endforeach

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            @error('internship')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.internship')</h4>

            @foreach($internships as $internship)
                <div class="filter__option-column">
                    <input
                        {{ old('internship') === $internship->id ? 'checked' : '' }}
                        id="{{ $internship->id }}"
                        class="filter__option-ui" type="radio" value="{{ $internship->id }}"
                        name="internship">
                    <label for="{{ $internship->id }}"
                           class="filter__option-title">{{ $internship->title }}</label>
                </div>
            @endforeach

        </div>
    </div>

    <div class="admin__column">
        <div class="filter__option">
            @error('document')
            <div class="message">{{ $message }}</div>
            @enderror

            <h4 class="filter__option-subtitle">@lang('summary.help_with_document')</h4>

            @foreach($documents as $document)
                <div class="filter__option-column">
                    <input
                        {{ old('document') === $document->id ? 'checked' : '' }}
                        id="{{ $document->id  }}"
                        class="filter__option-ui" type="radio"
                        value="{{ $document->id }}"
                        name="document">
                    <label for="{{ $document->id }}"
                           class="filter__option-title">{{ $document->title }}</label>
                </div>
            @endforeach

        </div>
    </div>
</div>

@isset($vacancy->start_salary)
    <small class="price__currency">от</small>
    <strong class="price__number">{{ $vacancy->start_salary }} BYN</strong>
@endisset

@isset($vacancy->end_salary)
    <small class="price__currency">до</small>
    <strong class="price__number">{{ $vacancy->end_salary }} BYN</strong>
@endisset

<div class="summary__cover summary__cover_mb">
    @if(isset($summary->applicant->photo) && !empty($summary->applicant->photo))
        <img class="summary__cover-pic summary__cover-pic_cover"
             src="{{ asset('uploads/' .  $summary->applicant->photo) }}"
             alt="">
    @else
        <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}"
             alt="">
    @endif
</div>
<div class="summary__data">
    <a href="{{ route('summary.show', ['id' => $summary->id])  }}"
       class="summary__data-link summary__data-link_mb">{{ $summary->title }}</a>
    <div class="summary__meta">
        @foreach($summary->residences as $residence)
            <small class="summary__meta-text">{{ $residence->title }}</small>
        @endforeach
    </div>
</div>
<div class="price">
    <div class="price__column">
        <time class="summary__date">{{ date('d-m-Y', strtotime($summary->created_at)) }}</time>
    </div>
</div>

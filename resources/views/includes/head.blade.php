<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="{{ URL::asset('assets/styles/css.css') }}">
<link rel="icon" type="image/png" href="{{ URL::asset('favicon-32x32.png') }}" sizes="32x32" />
<link rel="icon" type="image/png" href="{{ URL::asset('favicon-16x16.png') }}" sizes="16x16" />
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="WORKDRIVE24.BY"/>
<meta property="og:title" content="Работа водителем — свежие вакансии по всей Беларуси | WORKDRIVE24"/>
<meta property="og:description" content="@yield('description', 'Работа водителем — свежие вакансии в Беларуси для водителей любой категории: B, C, D, E. WORKDRIVE24 — уникальный сервис, который позволяет следить за вакансиями водителей, водителей-международников от различных компаний, а прямым работодателям размещать актуальные вакансии. Регистрируйтесь!')"/>
<meta property="og:image" content="{{ URL::asset('og-image.jpg') }}"/>
<meta property="og:image:type" content="image/jpeg"/>
<meta property="og:image:width" content="600"/>
<meta property="og:image:height" content="360"/>
<meta name="description" content="@yield('description', 'Работа водителем — свежие вакансии в Беларуси для водителей любой категории: B, C, D, E. WORKDRIVE24 — уникальный сервис, который позволяет следить за вакансиями водителей, водителей-международников от различных компаний, а прямым работодателям размещать актуальные вакансии. Регистрируйтесь!')">
<title>@yield('title', 'WORKDRIVE24 — Поиск работы для водителей')</title>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PMTKQ8L');</script>
<!-- End Google Tag Manager -->

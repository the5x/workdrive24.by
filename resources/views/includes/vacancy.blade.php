<div class="vacancy__item {{ \Carbon\Carbon::now() < $vacancy->deadline ? 'summary__item_yellow' : '' }}">
    <div class="vacancy__cover"
         style="background-image: url({{ asset('uploads/' .  $vacancy->company->photo) }})"></div>

    @if($vacancy->is_direct_employer)
        <div class="description__badge">
            @lang('summary.direct_employer')
        </div>
    @endif

    <a href="{{ route('vacancy.show', ['id' => $vacancy->id]) }}" class="vacancy__link">{{ $vacancy->title }}</a>

    <small class="vacancy__meta">
        <a class="vacancy__meta-link" href="{{ route('company.show', ['id' => $vacancy->company->id]) }}">{{ $vacancy->company->title }}</a>
    </small>
    <small class="vacancy__meta">
        @foreach($vacancy->residences as $residence)
            <span class="vacancy__meta-city">{{ $residence->title }}</span>
        @endforeach
    </small>
    <div class="vacancy__data">
        <div class="vacancy__price">

            <div class="price__column">
                @include('includes.start-end-salary')
            </div>

        </div>
    </div>
</div>

@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home') }}">Главная</a>
            <h3 class="section__title section__title_sm">@lang('summary.create_resume')</h3>

            <form action="{{ route('summary.create') }}" method="POST">
                @csrf

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('title')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">@lang('summary.title')</label>
                        <input class="admin__input" type="text" name="title" value="{{ old('title') }}"/>
                    </div>
                </div>

                @include('includes.summary-vacancy-options-when-create')

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('information')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">@lang('summary.additional_data')</h4>
                        <textarea class="admin__textarea" name="information" id="" cols="30"
                                  rows="10">{{ old('information') }}</textarea>
                    </div>
                </div>

                <button class="filter__button" type="submit">@lang('summary.create_resume')</button>
            </form>
        </section>
    </div>
@stop

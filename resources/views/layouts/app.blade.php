<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
    <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '654969725867560');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id=654969725867560&ev=PageView
&noscript=1"/>
        </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMTKQ8L"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="header">
    <div class="wrapper">
        @include('includes.header')
    </div>
</header>
<section class="hero" style="background-image: url({{ URL::asset('assets/images/slider.jpg') }})">
    <div class="wrapper">
        @yield('hero')
    </div>
</section>
<section class="banner">
    <div class="wrapper">
        @include('includes.banners')
    </div>
</section>
<section class="partners">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="{{ route('company.all')  }}">@lang('title.companies')</a></h3>
        @yield('partners')
    </div>
</section>
<section class="vacancy">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="{{ route('vacancies')  }}">@lang('title.vacancies')</a></h3>
        @yield('vacancy')
    </div>
</section>
<section class="banner">
    <div class="wrapper">
        @include('includes.banners')
    </div>
</section>
<section class="summary">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="{{ route('summaries')  }}">@lang('title.cv')</a></h3>
        @yield('summary')
    </div>
</section>
<section class="about">
    <div class="wrapper">
        @yield('about')
    </div>
</section>
<footer class="footer">
    <div class="wrapper">
        @include('includes.footer')
    </div>
</footer>
</body>
</html>

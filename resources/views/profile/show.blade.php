@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <h3 class="section__title section__title_sm">
            @lang('profile.user_profile')
            <a class="link__ico" href="{{ route('profile.update', ['id' => $profile->id])  }}">
                <img class="link__ico-pic" src="{{ URL::asset('assets/images/ico-edit.svg') }}" alt=""/>
            </a>
        </h3>

        @if(isset(Auth::user()->is_admin))

            @if($profile instanceof \App\Models\Employer)

                @if($profile->is_premium)
                    <form action="{{ route('profile.premium', ['id' => $profile->id]) }}" method="post">
                        @method('PUT')
                        @csrf

                        <button class="btn__premium">
                            <img class="btn__premium-ico" src="{{ URL::asset('assets/images/ico-premium.svg') }}"
                                 alt=""/>
                            <span class="btn__premium-text">Выключить премиум аккаунт</span>
                        </button>
                    </form>
                @else
                    <form action="{{ route('profile.premium', ['id' => $profile->id]) }}" method="post">
                        @method('PUT')
                        @csrf

                        <button class="btn__premium">
                            <img class="btn__premium-ico btn__premium-ico_opacity"
                                 src="{{ URL::asset('assets/images/ico-premium.svg') }}" alt=""/>
                            <span class="btn__premium-text btn__premium-text_opacity">Включить премиум аккаунт</span>
                        </button>
                    </form>
                @endif

            @endif

        @endif

        <section class="description">
            <div class="profile__grid">
                @if($profile->photo)
                    <div class="profile__column profile__column_sm">
                        <img class="profile__pic" src="{{ asset('uploads/' . $profile->photo)}}" alt="">
                    </div>
                @else
                    <div class="profile__column profile__column_sm profile__column_border"></div>
                @endif

                <div class="profile__column">
                    @auth('applicant')
                        <small class="profile__badge">@lang('profile.applicant')</small>
                    @endauth

                    @auth('employer')
                        <small class="profile__badge">@lang('profile.employer')</small>
                    @endauth

                    <span class="profile__meta-text">@lang('profile.initials'):
                        @if(isset($profile->firstname, $profile->lastname))
                            {{ $profile->firstname }} {{ $profile->lastname }}
                        @else
                            <em>@lang('profile.no_data_available')</em>
                        @endif
                    </span>

                    <span class="profile__meta-text">@lang('profile.phone_number'):
                        @if(isset( $profile->phone ))
                            <a class="profile__meta-link" href="tel:{{ $profile->phone }}">{{ $profile->phone }}</a>
                        @else
                            <em>@lang('profile.no_data_available')</em>
                        @endif
                    </span>

                    <span class="profile__meta-text">@lang('profile.email'):
                        @if(isset($profile->email))
                            <a class="profile__meta-link" href="mailto:{{ $profile->email }}">{{ $profile->email }}</a>
                        @else
                            <em>@lang('profile.no_data_available')</em>
                        @endif
                    </span>
                </div>
                <div class="profile__column">

                    @if($profile->summaries)
                        <h3 class="section__title section__title_sm">@lang('profile.created_resumes')</h3>

                        @foreach($profile->summaries as $summary)
                            <div class="profile__summary">
                                <a class="profile__meta-text profile__meta-link"
                                   href="{{ route('summary.show', ['id' => $summary->id]) }}">{{ $summary->title }}</a>
                            </div>
                        @endforeach
                    @endif

                    <br><br>

                    @if($profile->companies)
                        <h3 class="section__title section__title_sm">@lang('profile.created_companies')</h3>

                        @foreach($profile->companies as $company)
                            <div class="profile__summary">
                                <a class="profile__meta-text profile__meta-link"
                                   href="{{ route('company.show', ['id' => $company->id]) }}">{{ $company->title }}</a>
                            </div>
                        @endforeach
                    @endif

                    <br><br>

                    @if($profile->vacancies)
                        <h3 class="section__title section__title_sm">@lang('profile.created_vacancies')</h3>
                        @foreach($profile->vacancies as $vacancy)
                            <div class="profile__summary">
                                <a class="profile__meta-text profile__meta-link"
                                   href="{{ route('vacancy.show', ['id' => $vacancy->id]) }}">{{ $vacancy->title }}</a>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </section>
    </div>

@stop


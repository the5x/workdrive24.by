<?php

return [

    'vacancies' => 'Vacancies',
    'summaries' => 'Summaries',
    'companies' => 'Companies',
    'title' => 'There is a job for everyone',

    'create_cv' => 'Create CV',
    'create_company' => 'Create company',
    'create_vacancy' => 'Create vacancy',

];

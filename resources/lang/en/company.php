<?php

return [

    'about' => 'About the company',
    'vacancies' => 'Company vacancies',

    'create_company' => 'Create a company',
    'company_logo' => 'Company logo',
    'photo_param' => 'Max dimension size 1100 x 1100',
    'title' => 'Title',
    'company_information' => 'Company Information',
    'update' => 'Update'
];

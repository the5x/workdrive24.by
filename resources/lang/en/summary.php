<?php

return [

    'contact_person' => 'Contact person',
    'basic_data' => 'Basic data',
    'driver_license' => "Driver's license",
    'additional_document' => 'Additional documents',
    'place_residence' => 'City',
    'car_type' => 'Car type',
    'internship' => 'Internship',
    'language' => 'Languages',
    'help_with_document' => 'Help with documents',
    'work_experience' => 'Work experience',
    'additional_data' => 'Additional data',
    'phone_number' => 'Phone number',
    'email' => 'Email',
    'no_data_available' => 'No data available',

    'vacancies' => 'Vacancies',
    'find_resume' => 'Find CV',
    'find_vacancies' => 'Find Vacancies',
    'selection' => 'Selection',
    'summary' => 'Summary',
    'splash_title' => 'Viewing resumes for employers on a paid basis',
    'message' => 'Viewing a CV is allowed after purchasing a special package. Read more in the section "Tariffs and services"',

    'create_vacancy' => 'Create a vacancy',
    'title' => 'Title',
    'company' => 'Company',
    'salary_from' => 'Salary — from',
    'salary_to' => 'Salary — to',
    'we_provide' => 'What do we provide',
    'direct_employer' => 'Direct employer',

    'create_resume' => 'Сreate a resume',

    'reset' => 'All inclusive',

    'update_vacancy' => 'Update a vacancy',
    'delete_vacancy' => 'Delete a vacancy',

    'necessarily_item_1' => 'To place an ad, you must:',
    'necessarily_item_2' => 'Register your',
    'necessarily_item_2_link' => 'the company on the website',
    'necessarily_item_3' => 'Create a vacancy',
    'necessarily_item_4' => 'The vacancy will be linked to your company',
];

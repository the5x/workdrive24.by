<?php

return [

    'managing_and_creating' => 'Managing and creating',
    'company_logos' => 'Company logos',
    'companies' => 'Companies',
    'drivers_license' => "Driver's license",
    'additional_documents' => 'Additional documents',
    'place_of_residence' => 'Place of residence',
    'languages' => 'Languages',
    'car_type' => 'Car type',
    'work_experience' => 'Work experience',
    'internship' => 'Internship',
    'help_with_documents' => 'Help with documents',
    'premium_employers' => 'Premium employers',

];

<?php

return [
    'days' => 'dni',

    'rate_1_subtitle' => 'Najlepszy sposób na znalezienie nowych pracowników',
    'rate_1_list_1' => 'Publikowanie oferty pracy',
    'rate_1_list_2' => 'Zapoznaj się z ogłoszeniami kandydatów',

    'rate_2_subtitle' => 'Chcesz wyróżnić swoją ofertę pracy?',
    'rate_2_list_1' => "Podkreślenie oferty pracodawcy kolorem. Podnieś swoją ofertę ponad resztę ogłoszeń",

    'rate_3_subtitle' => 'Zostań partnerem firmy workdrive24',
    'rate_3_list_1' => "Umieść logo swojej firmy na stronie głównej w bloku „Partnerzy”",

    'rate_4_subtitle' => 'Niech Cię zauważą najlepsi!',
    'rate_4_list_1' => 'Umieszczenie banera reklamowego.',

    'rate_5_subtitle' => 'Wszystko w cenie!',
    'rate_5_list_1' => 'Publikowanie oferty pracy',
    'rate_5_list_2' => 'Zobacz ogłoszenia kandydatów, dla pracodawcy',
    'rate_5_list_3' => 'Umieść logo swojej firmy na stronie głównej w bloku „Partnerzy”',
    'rate_5_list_4' => 'Wyróżnianie ofert pracy',

    'description_1_title' => 'Wyświetlanie CV',
    'description_1_subtitle' => 'Zobacz ogłoszenia kandydatów',

    'description_2_title' => 'Podkreślenie reklamy',
    'description_2_subtitle' => 'Potrzebujesz pracowników do swojej firmy?',

    'description_3_title' => 'Umieść baner',
    'description_3_subtitle' => 'Spraw, aby ci najlepsi zwrócili na Ciebie uwagę!',

    'description_4_title' => 'Umieść logo firmowy',
    'description_4_subtitle' => 'Spraw, aby ci najlepsi zwrócili na Ciebie uwagę!',

    'write_to_us' => 'Wyślij do nas e-mail',
];

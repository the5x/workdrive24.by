<?php

return [

    'employers' => 'Pracodawca',
    'applicants' => 'Kandydat',
    'info' => 'Informacje',
    'portal_rules' => 'Regulamin portalu',

    'post_a_job' => 'Zamieść pracę',
    'rates_and_services' => 'Opłaty i usługi',
    'post_a_cv' => 'Zamieść CV',
    'contacts' => 'Kontakt',
    'user_agreement' => 'Umowa użytkownika',
    'public_contract' => 'Umowa publiczna',
    'privacy_policy' => 'Polityka Prywatności',
    'гules_for_posting_vacancies_and_cv' => 'Zasady zamieszczania ofert pracy i CV',

];

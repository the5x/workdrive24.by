<?php

return [

    'update_your_profile' => 'Zaktualizować profil',
    'photo' => 'Fota',
    'name' => 'Ime',
    'surname' => 'Nazwisko',
    'phone_number' => 'Numer telefonu',
    'update_profile' => 'Zaktualizować profil',

    'user_profile' => 'Profil użytkownika',
    'applicant' => 'Kandydat',
    'employer' => 'Pracodawca',
    'initials' => 'Imię i nazwisko',
    'no_data_available' => 'Brak danych',
    'email' => 'Email',
    'created_resumes' => 'Utworzone CV',
    'created_companies' => 'Utworzone firmy',
    'created_vacancies' => 'Utworzone oferty pracy',

];

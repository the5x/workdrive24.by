<?php

return [

    'vacancies' => 'Wakat',
    'summaries' => 'Podsumowanie',
    'companies' => 'Firmy',
    'title' => 'Praca dla każdego',

    'create_cv' => 'Utworz CV',
    'create_company' => 'Załóż firmę',
    'create_vacancy' => 'Utwórz ofertę pracy',

];

<?php

return [

    'about' => 'O firmie',
    'vacancies' => 'Oferty pracy',

    'create_company' => 'Załóż firmę',
    'company_logo' => 'Logo firmy',
    'photo_param' => 'Maksymalny rozmiar 1100 x 1100',
    'title' => 'Nazwa',
    'company_information' => 'Informacje o firmie',
    'update' => 'Zaktualizować'
];

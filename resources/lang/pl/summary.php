<?php

return [

    'contact_person' => 'Osoba kontaktowa',
    'basic_data' => 'Podstawowe dane',
    'driver_license' => 'Dokument tożsamości',
    'additional_document' => 'Dodatkowe dokumenty',
    'place_residence' => 'Miasto',
    'car_type' => 'Typ samochodu',
    'internship' => 'Praktyka',
    'language' => 'Języki',
    'help_with_document' => 'Pomoc z dokumentami',
    'work_experience' => 'Doświadczenie zawodowe',
    'additional_data' => 'Dodatkowe informacje',
    'phone_number' => 'Numer telefonu',
    'email' => 'Email',
    'no_data_available' => 'Brak danych',

    'vacancies' => 'Wakat',
    'find_resume' => 'Znajdź CV',
    'find_vacancies' => 'Znajdź oferty pracy',
    'selection' => 'Dobór',
    'summary' => 'Podsumowanie',
    'splash_title' => 'Zobacz CV dla pracodawców na zasadzie odpłatności',
    'message' => 'Wznowienie dostępu jest dozwolone po zakupie specjalnego pakietu. Przeczytaj więcej tutaj: Ceny',

    'create_vacancy' => 'Utwórz ofertę pracy',
    'title' => 'Nagłówek',
    'company' => 'Spółka',
    'salary_from' => 'Wynagrodzenie od',
    'salary_to' => 'Wynagrodzenie do',
    'we_provide' => 'Co zapewniamy',
    'direct_employer' => 'Bezpośredni pracodawca',

    'create_resume' => 'Utwórz CV',

    'reset' => 'All inclusive',

    'update_vacancy' => 'Aktualizacja oferty pracy',
    'delete_vacancy' => 'Usuń ofertę pracy',

    'necessarily_item_1' => 'Aby zamieścić ogłoszenie należy:',
    'necessarily_item_2' => 'Zarejestruj swoją',
    'necessarily_item_2_link' => 'firmę na stronie',
    'necessarily_item_3' => 'Utwórz ofertę pracy',
    'necessarily_item_4' => 'Oferta pracy będzie powiązana z Twoją firmą',
];

<?php

return [

    'managing_and_creating' => 'Zarządzanie i tworzenie',
    'company_logos' => 'Logotypy firm',
    'companies' => 'Spółka',
    'drivers_license' => 'Prawo jazdy',
    'additional_documents' => 'Dodatkowe dokumenty',
    'place_of_residence' => 'Miejsce zamieszkania',
    'languages' => 'Język',
    'car_type' => 'Typ auto',
    'work_experience' => 'Doświadczenie zawodowe',
    'internship' => 'Staż',
    'help_with_documents' => 'Pomoc z dokumentami',
    'premium_employers' => 'Pracodawcy Premium',

];

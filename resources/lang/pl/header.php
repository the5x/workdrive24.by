<?php

return [

    'login' => 'Logowanie',
    'registration' => 'Rejestracja',
    'profile' => 'Profil',
    'logout' => 'Wyjść',

];

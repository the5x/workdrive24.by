<?php

return [

    'companies' => 'Компании',
    'vacancies' => 'Вакансии',
    'cv' => 'Резюме',
    'about_us' => 'О нас',
    'rates' => 'Тарифы',

];

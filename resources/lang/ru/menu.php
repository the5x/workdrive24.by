<?php

return [

    'managing_and_creating' => 'Управление и создание',
    'company_logos' => 'Логотипы компаний',
    'companies' => 'Компании',
    'drivers_license' => 'Водительское удостоверение',
    'additional_documents' => 'Дополнительные документы',
    'place_of_residence' => 'Место жительства',
    'languages' => 'Язык',
    'car_type' => 'Тип авто',
    'work_experience' => 'Опыт работы',
    'internship' => 'Стажировка',
    'help_with_documents' => 'Помощь с документами',
    'premium_employers' => 'Премиальные работадатели',

];

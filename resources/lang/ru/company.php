<?php

return [

    'about' => 'О компании',
    'vacancies' => 'Вакансии компании',

    'create_company' => 'Создать компанию',
    'company_logo' => 'Логотип компании',
    'photo_param' => 'Максимальный размер 1100 x 1100',
    'title' => 'Название компании',
    'company_information' => 'Информация о компании',
    'update' => 'Обновить'
];

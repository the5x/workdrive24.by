<?php

return [

    'vacancies' => 'Вакансий',
    'summaries' => 'Резюме',
    'companies' => 'Компаний',
    'title' => 'Работа водителем — свежие вакансии для каждого',

    'create_cv' => 'Создать CV',
    'create_company' => 'Создать компанию',
    'create_vacancy' => 'Создать вакансию',

];

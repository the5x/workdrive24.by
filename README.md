1. git clone https://gitlab.com/the5x/workdrive.git
2. cd workdrive
3. composer install
4. npm install
5. cp .env.example .env
6. php artisan key:generate
7. Add your database config in the .env file
8. php artisan migrate
9. php artisan serve